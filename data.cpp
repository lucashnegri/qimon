#include "data.h"
#include "scaleinfo.h"

#include <Eigen/Dense>
#include <algorithm>

Data::Data(int n_points) : x(n_points), y(n_points)
{
}

void Data::calcBounds()
{
    x_from = x_to = calcBounds(x);
    y_from = y_to = calcBounds(y);
}

void Data::scaleX(const ScaleInfo &to)
{
    scale(x, x_from, to);
    x_to = to;
}

void Data::scaleY(const ScaleInfo &to)
{
    scale(y, y_from, to);
    y_to = to;
}

double Data::meanValue() const
{
    double acc = 0;

    for(int i = 0, e = y.size(); i < e; ++i)
        acc += y[i];

    return acc / y.size();
}

QVector<double> Data::detectPeaks(double rising_thres, double falling_thres, int n_points) const
{
    double mean = meanValue();
    int width = (n_points - 1) / 2;

    /* proportional to the mean count */
    rising_thres *= mean;
    falling_thres *= mean;

    bool onpeak = false;
    QVector<double> peaks;

    int a, b;

    for(int i = 0, e = x.size(); i < e; ++i)
    {
        double count = std::max(0.0, y[i] - mean);

        if(!onpeak)  // not on a peak region
        {
            // check if we entered in a peak region
            if(count > rising_thres)
            {
                onpeak = true;
                a = b = i;
            }
        }
        else   // on a peak region
        {
            if(count > rising_thres)
            {
                b = i;
            }
            else if(count < falling_thres)
            {
                // we exited from the peak region. Start the peak computation.
                onpeak = false;

                int max_idx = findMaximum(a, b);
                a = std::max(max_idx - width, 0);
                b = std::min(max_idx + width, x.size() - 1);

                double peak = fitGaussian(a, b);
                peaks.push_back(x_to.scale(peak, x_from));
            }
        }
    }

    return peaks;
}

static Eigen::MatrixXd make_vandermonde(int m, int o, double cond)
{
    Eigen::MatrixXd V(m, o);

    for(int i = 0; i < m; ++i)
    {
        const double x = (cond / m) * (i+1);
        double v = 1.0;

        for(int j = 0; j < o; ++j)
        {
            V(i, j) = v;
            v *= x;
        }
    }

    return V;
}

void Data::removeBaseline(int order, int max_it)
{
    const int m = y.size();
    ++order; // includes a constant too!

    // copy the y QVector to an Eigen vector
    Eigen::VectorXd yv(m);

    for(int i = 0; i < m; ++i)
        yv[i] = y[i];

    const double cond = std::pow(yv.maxCoeff(), 1.0 / (order-1));
    Eigen::MatrixXd V = make_vandermonde(m, order, cond);   // Vandermonde matrix
    Eigen::VectorXd a = Eigen::MatrixXd::Ones(order, 1);    // Initial coefficients
    Eigen::VectorXd b = yv;                                 // Initial baseline

    Eigen::HouseholderQR<Eigen::MatrixXd> QR = V.householderQr();

    for(int it = 0; it < max_it; ++it)
    {
        Eigen::VectorXd new_a = QR.solve(yv);

        bool converged = (new_a - a).norm() / a.norm() < 1e-3;
        a = new_a;
        b = V * new_a;

        if(converged)
            break;

        // this step prevents fitting the peaks
        for(int i = 0; i < m; ++i)
            yv[i] = std::min(yv[i], b[i]);
    }

    // remove the baseline
    for(int i = 0; i < m; ++i)
        y[i] -= b[i];
}

void Data::setRange(double min, double max)
{
    QVector<double> new_x, new_y;
    for(int i = 0, e = x.size(); i < e; ++i)
    {
        if(x[i] >= min && x[i] <= max)
        {
            new_x.append(x[i]);
            new_y.append(y[i]);
        }
    }

    // certify that the new data have at least 1 value (when possible)
    if(new_x.empty() && !x.empty())
    {
        new_x.append(x[0]);
        new_y.append(y[0]);
    }

    x.swap(new_x);
    y.swap(new_y);
}

void Data::checkSize(int size)
{
   if(x.size() != size)
   {
       x.resize(size);
       y.resize(size);
   }
}

/* private */

void Data::scale(QVector<double> &data, const ScaleInfo &from, const ScaleInfo &to) const
{
    for(int i = 0, e = data.size(); i < e; ++i)
        data[i] = from.scale(data[i], to);
}

ScaleInfo Data::calcBounds(const QVector<double> &data) const
{
    double smin = 1e9, smax = 1e-9;

    for(int i = 0, e = data.size(); i < e; ++i)
    {
        smin = std::min(smin, data[i]);
        smax = std::max(smax, data[i]);
    }

    return ScaleInfo(smin, smax);
}

int Data::findMaximum(int a, int b) const
{
    int p = a;

    for(; a <= b; ++a)
        if(y[a] > y[p])
            p = a;

    return p;
}

/* gaussian fitting */
static double gaussian(double x, double a, double c, double d)
{
    double t = (c - x);
    return a * exp(-t * t / (2 * d * d));
}

/* Derivatives */

static double gaussian_da(double x, double a, double c, double d)
{
    Q_UNUSED(a);

    double t = (c - x);
    return exp(-t * t / (2 * d * d));
}

static double gaussian_dc(double x, double a, double c, double d)
{
    double t = (x - c);
    return (a * t) / (d * d) * exp(-t * t / (2 * d * d));
}

static double gaussian_dd(double x, double a, double c, double d)
{
    double t = (x - c);
    return a * t * t / (d * d * d) * exp(-t * t / (2 * d * d));
}

double Data::fitGaussian(int a, int b) const
{
    int size = b - a + 1;

    Eigen::MatrixXd jacobian(size, 3);
    Eigen::Matrix3d I = Eigen::Matrix3d::Identity(3, 3);
    Eigen::VectorXd error(size);

    /* damping factor mu */
    double mu = 1, adj = 10;

    /* initial parameters */
    int m = (b - a) / 2 + a;
    Eigen::Vector3d p;
    p(0) = y[m];  // amplitude
    p(1) = x[m];  // center
    p(2) = 0.005; // width

    for(int it = 0; it < 30; ++it)
    {
        /* build the jacobian and error matrices */
        for(int n = a; n <= b; ++n)
        {
            jacobian(n - a, 0) = gaussian_da(x[n], p(0), p(1), p(2));
            jacobian(n - a, 1) = gaussian_dc(x[n], p(0), p(1), p(2));
            jacobian(n - a, 2) = gaussian_dd(x[n], p(0), p(1), p(2));
            error(n - a) = y[n] - gaussian(x[n], p(0), p(1), p(2));
        }

        /* compute the mse previous to the parameter update */
        double l_mse = error.squaredNorm() / size;
        Eigen::Vector3d l_p = p;

        /* compute the parameter changes */
        Eigen::Matrix3d hessian = jacobian.transpose() * jacobian;
        Eigen::Vector3d gradient = jacobian.transpose() * error;
        p.noalias() += (hessian + I * mu).colPivHouseholderQr().solve(gradient);

        /* evaluate the new parameters and adjust the damping factor accordingly */
        double n_mse = 0;

        for(int n = a; n <= b; ++n)
        {
            double err = (y[n] - gaussian(x[n], p(0), p(1), p(2)));
            n_mse += err * err;
        }

        n_mse /= size;

        if(n_mse <= l_mse)
        {
            // solution got better
            if(std::abs(n_mse - l_mse) < 1e-9)
                break;

            mu *= adj;
        }
        else
        {
            // solution got worst, revert
            p = l_p;
            mu /= adj;
        }
    }

    return p(1);
}
