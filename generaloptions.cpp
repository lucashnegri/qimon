#include "generaloptions.h"
#include "ui_generaloptions.h"
#include "utils.h"

#include <QSettings>
#include <QLineEdit>

GeneralOptions::GeneralOptions(QLineEdit *expName_, QWidget *parent) :
    QGroupBox(parent), ui(new Ui::GeneralOptions), expName(expName_)
{
    ui->setupUi(this);
}

GeneralOptions::~GeneralOptions()
{
    delete ui;
}

bool GeneralOptions::getRawSpectra() const
{
    return ui->checkRaw->isChecked();
}

void GeneralOptions::setRawSpectra(bool value)
{
    ui->checkRaw->setChecked(value);
}

bool GeneralOptions::getUseCurrentDir() const
{
    return ui->checkCurrentDir->isChecked();
}

void GeneralOptions::setUseCurrentDir(bool value)
{
    ui->checkCurrentDir->setChecked(value);
}

void GeneralOptions::loadSettings(QSettings &st)
{
    ui->checkRaw->setChecked(st.value("recordingRawSpectra").toBool());
    ui->checkCurrentDir->setChecked(st.value("recordingUseCurrentDir").toBool());
    // solver parameters are reset each time
}

void GeneralOptions::saveSettings(QSettings &st) const
{
    st.setValue("recordingRawSpectra", getRawSpectra());
    st.setValue("recordingUseCurrentDir", getUseCurrentDir());
}

void GeneralOptions::setSolverParams(const QVector<double> &params)
{
    ui->paramA->setValue(params[0]);
    ui->paramB->setValue(params[1]);
    ui->paramC->setValue(params[2]);
    ui->paramD->setValue(params[3]);
    ui->paramE->setValue(params[4]);
}

void GeneralOptions::setSolverLabels(const QVector<QString> &labels)
{
    ui->labelA->setText(labels[0]);
    ui->labelB->setText(labels[1]);
    ui->labelC->setText(labels[2]);
    ui->labelD->setText(labels[3]);
    ui->labelE->setText(labels[4]);
}

void GeneralOptions::setRecordingOptionsEnabled(bool value)
{
    ui->recording->setEnabled(value);
}

QVector<double> GeneralOptions::getSolverParams() const
{
    QVector<double> params =
    {
        ui->paramA->value(),
        ui->paramB->value(),
        ui->paramC->value(),
        ui->paramD->value(),
        ui->paramE->value()
    };

    return params;
}

QString GeneralOptions::getExperimentPath() const
{
    const QString name = expName->text().trimmed();

    if(name.isEmpty() || getUseCurrentDir())
        return name;
    else
        return getStorageLocation() + "/" + name;
}
