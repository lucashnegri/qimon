#ifndef MOVINGAVERAGE_H
#define MOVINGAVERAGE_H

#include <QVector>

/**
 * A simple and fast moving average filter.
 *
 * Accepts an input vector, restarting the filter when the size of the
 * input vector changes.
 */
class MovingAverage
{
public:
    MovingAverage(unsigned size = 30);
    const QVector<double>& update(const QVector<double>& data);
    const QVector<double>& getAverage();
    void reset();

private:
    void limit();

    QVector<double> acc, filtered;
    unsigned maxSize;
    unsigned n;
};

#endif // MOVINGAVERAGE_H
