QIMON
=====

About
-----

IMON reader, with a more modern user interface. On Windows, it uses the official programming
interface (HSSUSB2.dll). On Linux, it depends on [imon512e_ng][1] device driver. Features baseline
removal and peak detection algorithms.

Dependencies
------------

* HSSUSB2.dll on Windows, [imon512e_ng][1] on Linux
* Qt 4 or 5
* [Eigen library][2] at compile time

License
-------

GPLv3+

Author
------

Lucas Hermann Negri <lucashnegri@gmail.com>.
The [Linux device driver][1] was originally written by Alexander Lyasin <alexander.lyasin@gmail.com>

[1]: https://bitbucket.org/lucashnegri/imon512e2_ng
[2]: http://eigen.tuxfamily.org
