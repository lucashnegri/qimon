#ifndef RANGEDIALOG_H
#define RANGEDIALOG_H

#include <QDialog>
#include "qcustomplot.h"

namespace Ui
{
class RangeDialog;
}

/**
 * @brief Dialog for selecting the range of the _x_ and _y_ axes on plots.
 */
class RangeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RangeDialog(QWidget *parent = 0);
    ~RangeDialog();

    int exec(QCPRange &range_x, bool &auto_x, QCPRange &range_y, bool &auto_y);

private:
    Q_DISABLE_COPY(RangeDialog)
    Ui::RangeDialog *ui;
};

#endif // RANGEDIALOG_H
