#ifndef ADVANCEDPLOT_H
#define ADVANCEDPLOT_H

#include <QVector>
#include "qcustomplot.h"

class QPushButton;
class QWidget;
class QString;

/**
 * @brief Wraps a QCustomPlot with control buttons (range, save data) on a single widget.
 */
class AdvancedPlot : public QWidget
{
    Q_OBJECT
public:
    explicit AdvancedPlot(QWidget *parent = 0);

    /**
     * Configures the appearance of the plot.
     */
    void configurePlot(const QString& label_x, const QString& label_y, bool points);

    /**
     * Plots the data x by y.
     */
    void doPlot(const QVector<double> &x, const QVector<double> &y);

public slots:
    /**
     * Saves the points as a text file.
     */
    void saveData();

    /**
     * Saves the plot as an image file.
     */
    void saveImage();

    /**
     * Configures the range of the x and y axes
     */
    void setRange();

private:
    Q_DISABLE_COPY(AdvancedPlot)

    QVector<double> x, y;
    QCustomPlot *plot;

    QCPRange range_x, range_y;
    bool auto_x, auto_y;

    QPushButton *btnSetRange, *btnSaveData, *btnSaveImage;
};

#endif // ADVANCEDPLOT_H
