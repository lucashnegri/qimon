#include <QtGlobal>
#include "config.h"

#ifndef WITH_MOCK_IMON
#ifdef Q_OS_LINUX

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stropts.h>
#include <cstring>
#include <stdexcept>
#include <arpa/inet.h>

#include "data.h"
#include "libimon2.h"
#include "imon.h"

#define TempBytes 48
#define HeaderBytes 512
#define DataBytes 1024
#define DataSize 512

static const short capture_mode = 0x1;     // continuous mode?
static const short startcaptmode = 0x0100; // ??

IMON::IMON() : initialized(false), device_id(0)
{
}

IMON::~IMON()
{
    if(initialized)
        finalize();
}

// may throw std::runtime_error
void IMON::initialize(double exposureCycle, double exposureTime, const char *deviceName)
{
    if(initialized)
        finalize();

    /* open the device */
    device_id = open(deviceName, O_RDONLY);

    if(device_id < 0)
        throw std::runtime_error("Error when opening the device");

    try
    {
        initialized = true;

        /* set the capture mode */
        if(imon2_set_capturemode(device_id, htons(capture_mode)))
            throw std::runtime_error("Error setting the capture mode");

        /* configure the exposure cycle and time */
        double temp_double;

        over64((char *)&temp_double, (char *)&exposureCycle);

        if(imon2_set_exposurecycle(device_id, temp_double))
            throw std::runtime_error("Error setting the exposure cycle");

        over64((char *)&temp_double, (char *)&exposureTime);

        if(imon2_set_exposuretime(device_id, temp_double))
            throw std::runtime_error("Error setting the exposute time");
    }
    catch(const std::exception& e)
    {
        finalize();
        throw;
    }
}

// may throw std::runtime_error
void IMON::acquire(Data &data)
{
    static unsigned short buffer[DataSize];
    static char temp[TempBytes];

    /* data acquisition */
    {
        if(imon2_startcapture(device_id, htons(startcaptmode)))
            throw std::runtime_error("Error when starting the capture");

        if(imon2_begin_cycle(device_id, temp))
            throw std::runtime_error("Error when initiating the cycle");

        /* read the header */
        if(read(device_id, reinterpret_cast<char *>(buffer), HeaderBytes) < 0)
            throw std::runtime_error("Error when reading the header");

        /* read the data */
        if(read(device_id, reinterpret_cast<char *>(buffer), DataBytes) < 0)
            throw std::runtime_error("Error when reading the data");

        data.checkSize(DataSize);

        /* copy the data */
        for(int i = 0; i < DataSize; ++i)
        {
            data.x[i] = IMON_WAVELENGTHS[i] / 1000.0;
            data.y[DataSize - i - 1] = buffer[i]; // reverse order
        }

        /* end the cycle */
        if(imon2_end_cycle(device_id, temp))
            throw std::runtime_error("Error when ending the cycle");

        if(imon2_stopcapture(device_id))
            throw std::runtime_error("Error when stopping the capture");
    }

    /* clean-up */
    {
        if(imon2_begin_cycle(device_id, temp))
            throw std::runtime_error("Error on clean-up");

        if(imon2_end_header(device_id, temp))
            throw std::runtime_error("Error on clean-up");

        if(imon2_end_cycle(device_id, temp))
            throw std::runtime_error("Error on clean-up");
    }
}

void IMON::finalize()
{
    initialized = false;
    close(device_id);
}

#endif

#endif // WITH_MOCK_IMON
