#include "rangedialog.h"
#include "ui_rangedialog.h"

RangeDialog::RangeDialog(QWidget *parent) : QDialog(parent), ui(new Ui::RangeDialog)
{
    ui->setupUi(this);
}

RangeDialog::~RangeDialog()
{
    delete ui;
}

int RangeDialog::exec(QCPRange &range_x, bool &auto_x, QCPRange &range_y, bool &auto_y)
{
    // set
    ui->auto_x->setChecked(auto_x);
    ui->from_x->setValue(range_x.lower);
    ui->to_x->setValue(range_x.upper);

    ui->auto_y->setChecked(auto_y);
    ui->from_y->setValue(range_y.lower);
    ui->to_y->setValue(range_y.upper);

    int resp = QDialog::exec();

    // get
    if(resp == QDialog::Accepted)
    {
        range_x = QCPRange(ui->from_x->value(), ui->to_x->value());
        auto_x = ui->auto_x->isChecked();
        range_y = QCPRange(ui->from_y->value(), ui->to_y->value());
        auto_y = ui->auto_y->isChecked();
    }

    return resp;
}
