#ifndef PARAMSPINBOX_H
#define PARAMSPINBOX_H

#include <QDoubleSpinBox>

/**
 * @brief QDoubleSpinBox that formats the text with scientific notation.
 */
class ParamSpinBox : public QDoubleSpinBox
{
    Q_OBJECT
public:
    explicit ParamSpinBox(QWidget *parent = 0);
    QString textFromValue(double val) const;
};

#endif // PARAMSPINBOX_H
