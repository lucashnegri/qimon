#include "logger.h"
#include "generaloptions.h"
#include "utils.h"
#include "data.h"

#include <QTextStream>

Logger::Logger(GeneralOptions* options_, QObject *parent) :
    QObject(parent), options(options_)
{
}

Logger::~Logger()
{
    finish();
}

bool Logger::prepare()
{
    const QString path = options->getExperimentPath();

    if(path.isEmpty()) return true;

    peaksFile.setFileName(path + "_peaks");
    bool ok = peaksFile.open(QIODevice::Append);

    if(options->getRawSpectra())
    {
        rawFile.setFileName(path + "_raw");
        ok = ok && rawFile.open(QIODevice::Append);
    }

    return ok;
}

void Logger::appendPeaks(const QVector<double> &reference, const QVector<double> &current, int time)
{
    Q_UNUSED(reference);

    if(!peaksFile.isOpen()) return;

    QTextStream stream(&peaksFile);
    stream << time;

    for(int i = 0, e = current.size(); i < e; ++i)
        stream << " " << QString::number(current[i], 'f', 4);

    stream << "\n";
}

void Logger::appendRawData(const Data &data)
{
    if(!rawFile.isOpen()) return;

    QTextStream stream(&rawFile);

    for(int i = 0, e = data.x.size(); i < e; ++i)
    {
        stream << QString::number(data.x[i], 'f', 3) << " ";
        stream << QString::number((int)data.y[i])  << "\n";
    }

    stream << "\n";
}

void Logger::finish()
{
    peaksFile.close();
    rawFile.close();
}
