#ifndef SCALEINFO_H
#define SCALEINFO_H

/**
 * @brief Holds information about the scale of a data vector.
 */
class ScaleInfo
{
public:
    ScaleInfo();
    ScaleInfo(double min, double max);

    double scale(double val, const ScaleInfo &to) const;

private:
    double smin, smax;
};

#endif // SCALEINFO_H
