#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QFile>

#include "config.h"
#include "movingaverage.h"

class MassWindow;
class SignalMonitor;
class Data;
class QSettings;
class AcquisitionOptions;
class GeneralOptions;
class Logger;

namespace Ui
{
class MainWindow;
}

/**
 * @brief Handles the main GUI and coordinates interactions between the componentes of the application.
 *
 * Owns the single instances of SignalMonitor, Logger and all option objects instances
 * (objects that are shared with other parts of the system).
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void setToday();
    void signalPrepared(const Data& prepared);
    void peaksComputed(const QVector<double>& reference, const QVector<double>& current, int time);
    void workingChanged(bool working);

    // implemented by SignalMonitor. Are here just for exception handling
    void startRepeatedAcquisition();
    void stopRepeatedAcquisition();
    void singleAcquisition();

private:
    Q_DISABLE_COPY(MainWindow)

    void setupAcquisition();
    void displaySignalInfo(const QVector<double> &reference, const QVector<double> &current);
    void loadSettings(QSettings& st);
    void saveSettings(QSettings& st) const;

    MovingAverage avg;
    AcquisitionOptions* acqOptions;
    GeneralOptions* genOptions;
    SignalMonitor* monitor;
    Logger* logger;

#ifdef WITH_MASSVIEW
    void setupMassView();
    MassWindow* massWindow;
#endif

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
