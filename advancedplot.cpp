#include <QPushButton>
#include <QBoxLayout>
#include <QSpacerItem>
#include <QSizePolicy>
#include <QFileDialog>
#include <QWidget>
#include <QPushButton>
#include <QString>

#include "rangedialog.h"
#include "advancedplot.h"

AdvancedPlot::AdvancedPlot(QWidget *parent)
    : QWidget(parent), range_x(0, 1), range_y(0, 1), auto_x(true), auto_y(true)
{
    plot = new QCustomPlot();
    plot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    btnSetRange = new QPushButton(tr("Set &range"));
    btnSaveData = new QPushButton(tr("Save &data"));
    btnSaveImage = new QPushButton(tr("Save &image"));
    QSpacerItem *spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

    btnSaveData->setEnabled(false);
    btnSaveImage->setEnabled(false);

    QBoxLayout *buttons = new QBoxLayout(QBoxLayout::LeftToRight);
    buttons->addWidget(btnSetRange);
    buttons->addSpacerItem(spacer);
    buttons->addWidget(btnSaveData);
    buttons->addWidget(btnSaveImage);

    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->addWidget(plot);
    layout->addLayout(buttons);
    setLayout(layout);

    connect(btnSetRange, SIGNAL(clicked()), this, SLOT(setRange()));
    connect(btnSaveData, SIGNAL(clicked()), this, SLOT(saveData()));
    connect(btnSaveImage, SIGNAL(clicked()), this, SLOT(saveImage()));
}

void AdvancedPlot::configurePlot(const QString &label_x, const QString &label_y, bool points)
{
    plot->clearGraphs();
    plot->addGraph();
    plot->xAxis->setLabel(label_x);
    plot->xAxis->setRange(range_x);
    plot->yAxis->setLabel(label_y);
    plot->yAxis->setRange(range_y);
    plot->graph(0)->setPen(QPen(Qt::blue));

    if(points)
        plot->graph(0)
        ->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::blue, Qt::white, 3));
}

void AdvancedPlot::doPlot(const QVector<double> &x, const QVector<double> &y)
{
    plot->graph(0)->setData(x, y);

    if(auto_x)
        plot->xAxis->rescale();
    else
        plot->xAxis->setRange(range_x);

    if(auto_y)
        plot->yAxis->rescale();
    else
        plot->yAxis->setRange(range_y);

    plot->replot();

    this->x = x;
    this->y = y;

    btnSaveData->setEnabled(!x.empty() && !y.empty());
    btnSaveImage->setEnabled(!x.empty() && !y.empty());
}

void AdvancedPlot::saveData()
{
    QString name =
        QFileDialog::getSaveFileName(this, tr("Save data"), QString(), tr("Text data (*.txt)"));

    if(!name.isEmpty())
    {
        QFile file(name);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);

        for(int i = 0, e = x.size(); i < e; ++i)
        {
            out << x[i] << " " << y[i] << "\n";
        }
    }
}

void AdvancedPlot::saveImage()
{
    QString name =
        QFileDialog::getSaveFileName(this, tr("Save image"), QString(), tr("PDF (*.pdf)"));

    if(!name.isEmpty())
        plot->savePdf(name, true, plot->width(), plot->height());
}

void AdvancedPlot::setRange()
{
    RangeDialog d;
    int resp = d.exec(range_x, auto_x, range_y, auto_y);

    if(resp == QDialog::Accepted)
        doPlot(x, y);
}
