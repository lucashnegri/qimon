#ifndef SIGNALMONITOR_H
#define SIGNALMONITOR_H

#include <QObject>
#include <QTimer>
#include <QTime>
#include <QVector>

#include "data.h"

class QSettings;
class QLayout;
class AcquisitionOptions;
class IMON;
class MovingAverage;

/**
 * @brief Handles the acquisition and processing of IMON signals.
 */
class SignalMonitor : public QObject
{
    Q_OBJECT

public:
    explicit SignalMonitor(AcquisitionOptions* option_, QObject *parent = 0);
    ~SignalMonitor();
    bool isWorking() const;

    /**
     * @brief Returns the last computed peaks.
     */
    const QVector<double>& getCurrent();

    /**
     * @brief Returns the current reference peaks.
     */
    const QVector<double>& getReference();

    /**
     * @brief Returns the average peaks after _rep_ repetitions.
     */
    QVector<double> getAverage(int rep);

    void setReference(const QVector<double>& newReference);

signals:
    void workingChanged(bool working);
    void signalAcquired(const Data& raw);       // raw signal available
    void signalPrepared(const Data& prepared);  // preprocessed signal available (baseline removal)
    void peaksComputed(const QVector<double>& reference, const QVector<double>& current, int time);

public slots:
    void singleAcquisition(bool now = false);
    void startAcquisition();
    void stopAcquisition();
    void addReference();
    void clearReference();
private slots:
    void acquire();

private:
    Q_DISABLE_COPY(SignalMonitor)
    void setWorking(bool working);

    Data data;

    QTimer repeatTimer;
    QTime stopwatch;

    QVector<double> current;     // peaks

    MovingAverage* refAvg;       // destroyed on delete
    IMON* imon;                  // destroyed on delete
    AcquisitionOptions* options; // must be always available

    bool working;
    bool singleShoot;
};

#endif // SIGNALMONITOR_H
