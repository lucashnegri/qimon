#include "movingaverage.h"

MovingAverage::MovingAverage(unsigned size) : maxSize(size), n(0)
{
}

const QVector<double> &MovingAverage::update(const QVector<double> &data)
{
    if(acc.size() != data.size())
    {
        // restart
        acc = filtered = data;
        n   = 1;
    }
    else
    {
        // update internal state and filter the data vector
        ++n;

        for(unsigned i = 0, e = data.size(); i < e; ++i)
        {
            acc[i] += data[i];
            filtered[i] = acc[i] / n;
        }

        if(n >= maxSize)
            limit();
    }

    return filtered;
}

const QVector<double> &MovingAverage::getAverage()
{
    return filtered;
}

void MovingAverage::limit()
{
    const int orig_n = n;
    n = n / 3;
    const double div = orig_n / (double) n; // actual divisor ~ 3

    for(unsigned i = 0, e = acc.size(); i < e; ++i)
        acc[i] /= div;
}

void MovingAverage::reset()
{
    filtered.clear();
    acc.clear();
    n = 0;
}


