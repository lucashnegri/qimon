#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QVector>

class GeneralOptions;
class Data;

/**
 * @brief Records experimental data on files.
 */
class Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(GeneralOptions* options_, QObject *parent = 0);
    ~Logger();

    /**
     * Prepares to log into path. If path is empty, no logging is performed.
     *
     * Usage: logger.prepare( logger.getPath("myexp") );
     */
    bool prepare();

    /**
     * Returns the base path were the data for expName should be stored.
     */
    QString getPath(const QString& expName) const;

public slots:
    void appendPeaks(const QVector<double> &reference, const QVector<double> &current, int time);
    void appendRawData(const Data& data);
    void finish();

private:
    Q_DISABLE_COPY(Logger)

    QFile peaksFile, rawFile;
    GeneralOptions* options;
};

#endif // LOGGER_H
