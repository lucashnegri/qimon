#include "paramspinbox.h"

ParamSpinBox::ParamSpinBox(QWidget *parent) :
    QDoubleSpinBox(parent)
{
}

QString ParamSpinBox::textFromValue(double val) const
{
    return QString::number(val);
}
