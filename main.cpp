#include <QApplication>
#include <QMetaType>
#include <QVector>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("OProj");
    a.setApplicationName("QIMON");

    qRegisterMetaType<QVector<double> >("QVector<double>");

    MainWindow w;
    w.show();

    return a.exec();
}
