#include "scaleinfo.h"

ScaleInfo::ScaleInfo() : smin(1e9), smax(-1e9)
{
}

ScaleInfo::ScaleInfo(double min, double max) : smin(min), smax(max)
{
}

double ScaleInfo::scale(double val, const ScaleInfo &to) const
{
    return ((val - smin) / (smax - smin)) * (to.smax - to.smin) + to.smin;
}
