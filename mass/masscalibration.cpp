#include "masscalibration.h"

#include <QString>
#include <QFile>
#include <QTextStream>
#include <stdexcept>

MassCalibration::MassCalibration() : rows(0), cols(0), sensors(0)
{

}

MassCalibration::MassCalibration(int rows_, int cols_, int sensors_)
    : A(sensors_, rows_*cols_), rows(rows_), cols(cols_), sensors(sensors_)
{
    A.setZero();
}

void MassCalibration::load(const QString &path)
{
    QFile file(path);

    if(!file.open(QIODevice::ReadOnly))
        throw std::runtime_error("Could not open the file for reading");

    QTextStream inp(&file);
    inp >> rows >> cols >> sensors;

    if(rows <= 0 || rows > 999 || cols <= 0 || cols > 999 || sensors <= 0 || sensors > 999)
        throw std::runtime_error("Invalid calibration file");

    const int positions = getPositions();
    A = Eigen::MatrixXd(sensors, positions);

    for(int i = 0; i < sensors; ++i)
        for(int j = 0; j < positions; ++j)
            inp >> A(i, j);

    if(inp.status() != QTextStream::Ok)
        throw std::runtime_error("Invalid calibration file");
}

void MassCalibration::save(const QString &path) const
{
    QFile file(path);

    if(!file.open(QIODevice::WriteOnly))
        throw std::runtime_error("Could not open the file for writing");

    save(&file);
}

void MassCalibration::save(QFile *file) const
{
    QTextStream out(file);
    out << rows << " " << cols << " " << sensors << "\n";

    const int positions = getPositions();

    for(int i = 0; i < sensors; ++i)
    {
        for(int j = 0; j < positions; ++j)
            out << A(i, j) << " ";

        out << "\n";
    }
}

const Eigen::MatrixXd &MassCalibration::getMatrix() const
{
    return A;
}

Eigen::MatrixXd &MassCalibration::getMatrix()
{
    return A;
}

Eigen::VectorXd MassCalibration::getSensitivity() const
{
    return A.cwiseAbs().rowwise().mean();
}

int MassCalibration::getPositions() const
{
    return rows * cols;
}

int MassCalibration::getSensors() const
{
    return sensors;
}

int MassCalibration::getRows() const
{
    return rows;
}

int MassCalibration::getCols() const
{
    return cols;
}
