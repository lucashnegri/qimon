#ifndef MASSOPTIONS_H
#define MASSOPTIONS_H

#include <QGroupBox>
#include <QString>
#include "massdimension.h"

class QSettings;

namespace Ui
{
class MassOptions;
}

/**
 * @brief Handles and stores all options related to the mass viewer widget and calibration.
 */
class MassOptions : public QGroupBox
{
    Q_OBJECT

public:
    explicit MassOptions(QWidget *parent = 0);
    ~MassOptions();

    double        getScale()           const;
    MassDimension getDimension()       const;
    bool          getRecordData()      const;
    QString       getCalibrationPath() const;
    int           getSide()            const;
    int           getPadding()         const;
    int           getScaleWidth()      const;

    void setScale(double value);
    void setDimension(MassDimension value);
    void setRecordData(bool value);
    void setCalibrationPath(const QString& value);
    void setSide(int value);
    void setPadding(int value);
    void setScaleWidth(int value);

    void loadSettings(QSettings& st);
    void saveSettings(QSettings& st) const;

signals:
    void showCalibrationWizard();

private slots:
    void selectCalibrationPath();

private:
    Q_DISABLE_COPY(MassOptions)
    int side, padding, scaleWidth;

    QString calibrationPath;
    Ui::MassOptions *ui;
};

#endif // MASSOPTIONS_H
