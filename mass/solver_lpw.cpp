#include "config.h"

#ifdef WITH_LPW

#include "solver.h"
#include "generaloptions.h"
#include <lpw>

QVector<double> Solver::solve_it(const Eigen::MatrixXd& A, const QVector<double>& values, const QVector<double> &p)
{
    const int vars = A.cols();

    lpw::Solver solver(2 * vars);
    lpw::VD row = solver.makeRow();

    // Ax = b
    for(int i = 0; i < A.rows(); ++i)
    {
        for(int j = 0; j < vars; ++j)
            row[j] = A(i, j);

        solver.addConstraint(row, lpw::EQ, values[i]);
    }

    for(int i = 0; i < vars; ++i)
        solver.setBounds(i, p[0], p[1]);

    std::fill(row.begin(), row.end(), 0);

    // x > -t and x < t
    for(int i = 0; i < vars; ++i)
    {
        // x - t < 0
        row[i]       = 1;
        row[i + vars] = -1;
        solver.addConstraint(row, lpw::LE, 0);

        // x + t > 0
        row[i]       = 1;
        row[i + vars] =  1;
        solver.addConstraint(row, lpw::GE, 0);

        row[i] = row[i + vars] = 0;
    }

    // t
    std::fill(row.begin(), row.end(), 0);

    for(int i = vars; i < 2 * vars; ++i)
        row[i] = 1;

    solver.setObjective(row, false);

    double obj;
    solver.solve(row, obj);

    QVector<double> out(vars);

    for(int i = 0; i < vars; ++i)
        out[i] = qMax(row[i], 0.0);

    return out;
}

QVector<double> Solver::getDefaultParams()
{
    return QVector<double>{0.0, 1.0};
}

QVector<QString> Solver::getParamLabels()
{
    return QVector<QString>
    {
        QString("Lower limit"),
        QString("Upper limit")
    };
}

#endif
