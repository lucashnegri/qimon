#include "massview.h"
#include "massdimension.h"
#include "massoptions.h"

#include <QPainter>
#include <QColor>
#include <QRect>
#include <QLinearGradient>
#include <QTextOption>

MassView::MassView(QWidget *parent) : QWidget(parent), opt(0)
{
}

void MassView::setOptions(MassOptions *opt)
{
    this->opt = opt;
}

void MassView::setData(const MassData &data)
{
    if(!opt) return;

    origData = normData = data;
    normData.normalize(opt->getScale());
    update();
    updateGeometry();
}

QSize MassView::minimumSizeHint() const
{
    if(!opt) return QSize(0, 0);

    if(normData.getSize() == 0)
    {
        return QSize(opt->getSide(), opt->getSide());
    }
    else
    {
        int len = opt->getSide() + opt->getPadding();
        return QSize(normData.getCols() * len + opt->getScaleWidth(), normData.getRows() * len);
    }
}

void MassView::paintEvent(QPaintEvent *)
{
    if(!opt) return;

    QPainter painter(this);
    const QTextOption textOpt(Qt::AlignCenter);

    if(normData.getSize() == 0)
    {
        painter.drawText(rect(), tr("Waiting for data."), textOpt);
        return;
    }

    const int side          = opt->getSide();
    const int padding       = opt->getPadding();
    const int len           = side + padding;
    const int rows          = normData.getRows(), cols = normData.getCols();
    const MassDimension dim = opt->getDimension();

    // font
    QFont font = QFont();
    font.setBold(true);
    painter.setFont(font);
    painter.setPen(Qt::white);

    // draw the cells and optionally the raw values
    for(int i = 0; i < rows; ++i)
    {
        for(int j = 0; j < cols; ++j)
        {
            const QRect cell(j * len, i * len, side, side);
            const QColor color = colorFromValue(normData.at(i, j));

            painter.fillRect(cell, color);

            if(dim != DimensionNone)
            {
                QString text;
                const double val = origData.at(i, j);

                if(dim == DimensionGram)
                    text = QString::number((int)(val * 1000)) + " g";
                else
                    text = QString::number(val, 'f', 1) + " kg";

                painter.drawText(cell, text, textOpt);
            }
        }
    }

    // draw the gradient scale
    const QRect rect(cols * len, 0, opt->getScaleWidth(), (rows) * len - padding);

    QLinearGradient gradient(rect.topLeft(), rect.bottomLeft());
    const int np = 25; //  number of stop points

    for(int p = 0; p <= np; ++p)
    {
        const double a = p / (double)np;
        gradient.setColorAt(a, colorFromValue(a));
    }

    painter.fillRect(rect.left(), rect.top(), rect.width(), rect.height(), gradient);
}

QColor MassView::colorFromValue(double intensity) const
{
    return QColor::fromHslF((1.0 - intensity) * 0.61, 1.0, 0.4, 1.0);
}
