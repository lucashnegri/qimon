#ifndef MASSCALIBRATION_H
#define MASSCALIBRATION_H

#include <Eigen/Dense>

class QString;
class QFile;

/**
 * @brief Holds the calibration data, including the sensing matrix A.
 */
class MassCalibration
{
public:
    MassCalibration();
    MassCalibration(int rows_, int cols_, int sensors_);

    void load(const QString& path);
    void save(const QString& path) const;
    void save(QFile *file) const;

    const Eigen::MatrixXd& getMatrix() const;
    Eigen::MatrixXd& getMatrix();

    int getPositions() const;
    int getSensors() const;
    int getRows() const;
    int getCols() const;

    Eigen::VectorXd getSensitivity() const;
private:
    Eigen::MatrixXd A;
    int rows, cols, sensors;
};

#endif // MASSCALIBRATION_H
