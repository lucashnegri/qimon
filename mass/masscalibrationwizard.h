#ifndef MASSCALIBRATIONWIZARD_H
#define MASSCALIBRATIONWIZARD_H

#include <QWizard>
#include <QVector>
#include <QScopedPointer>
#include "movingaverage.h"

class QString;
class SignalMonitor;
class MassCalibration;
class QTimer;

namespace Ui
{
class MassCalibrationWizard;
}

/**
 * @brief Responsible for creating calibration files.
 */
class MassCalibrationWizard : public QWizard
{
    Q_OBJECT

public:
    explicit MassCalibrationWizard(QWidget *parent = 0);
    ~MassCalibrationWizard();

    bool validateCurrentPage();
    void setPath(const QString& path);
    void runWizard(SignalMonitor* monitor);

public slots:
    void done(int result);
    void reject();

protected:
    void initializePage(int id);

private slots:
    void acquirePosition();
    void clearPosition();
    void doAcquire();
    void positionChanged(int v);

private:
    Q_DISABLE_COPY(MassCalibrationWizard)

    bool validateLoadCreatePage();
    bool validateAcquisitionPage();
    void log(const QString& msg);
    void guide();

    void enableTimer();
    void disableTimer();
    void computeCalibration();

    QString calibrationPath;
    QVector<MovingAverage> avg; // holds the data acquired durint the calibration

    QScopedPointer<MassCalibration> calibration;

    SignalMonitor* monitor; // valid only during the call to runWizard()
    Ui::MassCalibrationWizard *ui;

    QTimer* timer;
};

#endif // MASSCALIBRATIONWIZARD_H
