#ifndef MASSVIEW_H
#define MASSVIEW_H

#include <QWidget>
#include "massdata.h"

class MassOptions;

/**
 * @brief Widget that shows values in a grid, with a color scale.
 */
class MassView : public QWidget
{
    Q_OBJECT

public:
    explicit MassView(QWidget *parent = 0);
    void setOptions(MassOptions* opt);
    void setData(const MassData& data);
    QSize minimumSizeHint() const;

protected:
    void paintEvent(QPaintEvent *evt);

private:
    Q_DISABLE_COPY(MassView)
    QColor colorFromValue(double intensity) const;

    MassData origData, normData;
    MassOptions* opt; // managed outside
};

#endif // MASSVIEW_H
