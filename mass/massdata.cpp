#include "massdata.h"
#include <algorithm>
#include <stdexcept>

MassData::MassData() : rows(0), cols(0), time(0)
{

}

MassData::MassData(const QVector<double> &values_, int rows_, int cols_, int time_)
    : values(values_), rows(rows_), cols(cols_), time(time_)
{
}

double MassData::at(int i, int j) const
{
    return values.at(i * cols + j);
}

int MassData::getSize() const
{
    return values.size();
}

int MassData::getRows() const
{
    return rows;
}

int MassData::getCols() const
{
    return cols;
}

int MassData::getTime() const
{
    return time;
}

void MassData::normalize(double max)
{
    if(values.empty() || (values.size() != (rows * cols)))
        throw std::runtime_error("Invalid values");

    if(max <= 0.0)
        max = *std::max_element(values.begin(), values.end());

    for(double& v : values)
        v = std::max(0.0, std::min(v / max, 1.0));
}

const QVector<double> &MassData::getValues() const
{
    return values;
}
