#include "solver.h"
#include "masscalibration.h"
#include "config.h"

#include <algorithm>
#include <limits>

Solver::Solver(QObject *parent) : QObject(parent), calibration(0), time(0), prepared(false)
{
}

void Solver::prepare(const QVector<double> &reference, const QVector<double> &current, int time,
                     const MassCalibration *calibration, const QVector<double> &params)
{
    const int ref_size = reference.size();
    const int cur_size = current.size();
    const int sensors  = calibration->getSensors();

    // initialize y, handling missing values (a missing reference or current value will set that
    // sensor value to 0.
    values.resize(sensors);

    prepA = calibration->getMatrix();
    const Eigen::VectorXd sens = calibration->getSensitivity();

    for(int i = 0, e = sensors; i < e; ++i) {
        values[i] = (i < ref_size && i < cur_size) ? current[i] - reference[i] : 0.0;

        // normalize the sensitivity
        prepA.row(i) /= sens[i];
        values[i] /= sens[i];
    }

    this->params      = params;
    this->time        = time;
    this->calibration = calibration;
    prepared          = true;
}

QVector<double> Solver::solve()
{
    QVector<double> out;

    try
    {
        if(!prepared || calibration->getSensors() != values.size())
            throw "";

        prepared = false;
        out = solve_it(prepA, values, params);
    }
    catch(...)
    {
    }

    emit finished(out, time);
    return out;
}


double limit(double val, double min, double max)
{
    return std::max(std::min(val, max), min);
}
