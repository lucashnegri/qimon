#include "masswindow.h"
#include "ui_masswindow.h"
#include "massdata.h"
#include "mainwindow.h"
#include "massoptions.h"
#include "masscalibrationwizard.h"
#include "masscalibration.h"
#include "solver.h"
#include "generaloptions.h"

#include <QMessageBox>
#include <QTextStream>
#include <QFile>
#include <QThread>

const int NUM_PARAMS = 5;

MassWindow::MassWindow(SignalMonitor *monitor_, GeneralOptions *options_, QWidget *parent) :
    QWidget(), // not a parant, to allow howing only the MassView window
    solver(new Solver()),
    thread(new QThread()),
    massCalibration(new MassCalibration()),
    ui(new Ui::MassWindow),
    wizard(new MassCalibrationWizard(parent)),
    options(new MassOptions()),
    monitor(monitor_),
    genOptions(options_),
    available(true)
{
    ui->setupUi(this);
    connect(thread, SIGNAL(started()), solver, SLOT(solve()));
    connect(solver, SIGNAL(finished(const QVector<double>&, int)), thread, SLOT(quit()));
    connect(solver, SIGNAL(finished(const QVector<double>&, int)),
            this   , SLOT(solverFinished(const QVector<double>&, int)));

    ui->view->setOptions(options);
    connect(options, SIGNAL(showCalibrationWizard()), this, SLOT(showCalibrationWizard()));

    QVector<QString> p_names = solver->getParamLabels();
    QVector<double> p_values = solver->getDefaultParams();

    while(p_names.size() < NUM_PARAMS)
        p_names.append(tr("Unused"));

    while(p_values.size() < NUM_PARAMS)
        p_values.append(0);

    genOptions->setSolverLabels(p_names);
    genOptions->setSolverParams(p_values);
}

MassWindow::~MassWindow()
{
    delete options; // QPointer: if options has a parent, it may already be deleted
    delete solver;
    delete thread;
    delete massCalibration;
    delete ui;
}

void MassWindow::peaksComputed(const QVector<double>& reference, const QVector<double>& current,
                               int time)
{
    if(!available || !isVisible())
        return;

    available = false;
    solver->prepare(reference, current, time, massCalibration, genOptions->getSolverParams());
    solver->moveToThread(thread);
    thread->start();
}

void MassWindow::toggle()
{
    if(isVisible())
        hide();
    else
        show();
}

void MassWindow::loadSettings(QSettings &st)
{
    options->loadSettings(st);
}

void MassWindow::saveSettings(QSettings &st) const
{
    options->saveSettings(st);
}

QWidget *MassWindow::getOptionsWidget()
{
    return options;
}

void MassWindow::show()
{
    if(loadCalibration())
        QWidget::show();
}

void MassWindow::solverFinished(const QVector<double>& mass, int time)
{
    if(mass.empty())
    {
        qWarning("solve() failed");
    }
    else
    {
        MassData data(mass, massCalibration->getRows(), massCalibration->getCols(), time);
        ui->view->setData(data);

        if(options->getRecordData())
            recordData(genOptions->getExperimentPath(), data);

        resize(0, 0);
    }

    available = true;
}

void MassWindow::showCalibrationWizard()
{
    wizard->setPath(options->getCalibrationPath());
    wizard->runWizard(monitor);
}

bool MassWindow::loadCalibration()
{
    const QString path = options->getCalibrationPath();

    try
    {
        massCalibration->load(path);
        return true;
    }
    catch(const std::exception& e)
    {
        QString msg(tr("Could not load the calibration file at\n") + path);
        msg += ".\n\n";
        msg += e.what();
        QMessageBox::warning(this, tr("Warning"), msg);
        return false;
    }
}

void MassWindow::recordData(const QString& path, const MassData& data) const
{
    if(!options->getRecordData() || path.isEmpty())
        return;

    QFile file(path + "_mass");

    if(!file.open(QIODevice::Append))
    {
        qDebug("Could not record the mass data.");
        return;
    }

    QTextStream ts(&file);
    ts << data.getTime();

    const QVector<double>& values = data.getValues();

    for(int i = 0, e = values.size(); i < e; ++i)
        ts << " " << values[i];

    ts << "\n";
}
