#include "config.h"

#ifdef WITH_DE

// differential evolution solver. Sizes must be choosen at compile time.

#include "solver.h"
#include "solver-de.hpp"
#include <array>
#include <algorithm>

const double eps = 1e-2;
const double P = 0.01;

struct SparseFitness
{
    double operator()(SolverDE::ConstRef cand) const
    {
        double norm = 0;

        for(double val: cand)
            norm += std::pow(std::abs(val), P);

        const Eigen::Map<Eigen::VectorXd> x(const_cast<double*>(&cand[0]), cand.size());
        return - ((A*x - y).squaredNorm() + sigma * norm);
    }

    Eigen::MatrixXd A;
    Eigen::VectorXd y;
    double sigma;
};

QVector<double> Solver::solve_it(const Eigen::MatrixXd& A, const QVector<double>& values,
                                 const QVector<double>& p)
{
    const int OBS = A.rows();
    const int POS = A.cols();

    // sane limits for the parameters
    const int n_pop    = limit(p[0], 10, 200);
    const double sigma = limit(p[1], 0.0, 1.0);
    const double upper = limit(p[2], 0.01, 100.0);
    const double cr    = limit(p[3], 0.1, 1.0);

    // create and configure the solver
    SolverDE::Solver<SparseFitness> solver(n_pop, POS);
    solver.f = DE_F;
    solver.cr = cr;

    for(int i = 0; i < POS; ++i)
    {
        solver.limits[i][0] = 0.0;
        solver.limits[i][1] = upper;
    }

    solver.fitness.A = A;
    solver.fitness.y.resize(OBS);
    solver.fitness.sigma = sigma;

    for(int i = 0; i < OBS; ++i)
        solver.fitness.y(i) = values[i];

    // run
    solver.run(DE_MAX, DE_STUCK);
    const auto best = std::get<1>(solver.best());

    // output
    QVector<double> out(POS);

    for(int i = 0, e = best.size(); i < e; ++i)
        out[i] = best[i];

    return out;
}

QVector<double> Solver::getDefaultParams()
{
    return QVector<double>{DE_POP, DE_SIGMA, DE_SUP, DE_CR};
}

QVector<QString> Solver::getParamLabels()
{
    return QVector<QString>
    {
        QString("Population"),
        QString("sigma"),
        QString("Upper limit"),
        QString("CR")
    };
}

#endif
