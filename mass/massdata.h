#ifndef MASSDATA_H
#define MASSDATA_H

#include <QVector>

/**
 * @brief Stores informations about mass on each cell.
 */
class MassData
{
public:
    MassData();

    /**
     * Constructor. Sets the current values, normalizing them.
     */
    MassData(const QVector<double>& values_, int rows_, int cols_, int time_);

    /**
     * Access the data at _idx_.
     */
    double at(int i, int j) const;

    /**
     * Normalizes the data to the 0 to 1 range, using the maximum value in the
     * data (when max <= 0) or max as end of scale.
     */
    void normalize(double max);

    int getSize() const;
    int getRows() const;
    int getCols() const;
    int getTime() const;
    const QVector<double>& getValues() const;

private:
    QVector<double> values;
    int rows, cols, time;
};

#endif // MASSDATA_H
