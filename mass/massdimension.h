#ifndef MASSDIMENSION_H
#define MASSDIMENSION_H

enum MassDimension
{
    DimensionNone,
    DimensionGram,
    DimensionKilogram
};

#endif // MASSDIMENSION_H
