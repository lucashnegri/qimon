#ifndef MASSWINDOW_H
#define MASSWINDOW_H

#include <QWidget>
#include <QString>
#include <QPointer>

// forward declarations
namespace Ui
{
class MassWindow;
}

class MassOptions;
class MassCalibration;
class MassData;
class MassCalibrationWizard;
class QSettings;
class Solver;
class QThread;
class SignalMonitor;
class GeneralOptions;

/**
 * @brief Shows the mass on each cell, holding the mass-related objects.
 */
class MassWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MassWindow(SignalMonitor* monitor_, GeneralOptions* options_, QWidget* parent = 0);
    ~MassWindow();

public slots:
    void peaksComputed(const QVector<double>& reference, const QVector<double>& current, int time);

    /**
     * Shows / hides the mass distribution view window, always reloading the sensor
     * configuration file on show.
     */
    void show();
    void toggle();

    void loadSettings(QSettings& st);
    void saveSettings(QSettings& st) const;

    QWidget* getOptionsWidget();

private slots:
    void solverFinished(const QVector<double>& mass, int time);
    void showCalibrationWizard();

private:
    Q_DISABLE_COPY(MassWindow)

    bool loadCalibration();
    void recordData(const QString& path, const MassData& data) const;

    // managed by the destructor
    Solver* solver;
    QThread* thread;
    MassCalibration* massCalibration;
    Ui::MassWindow* ui;

    // managed by the parent
    MassCalibrationWizard* wizard;
    QPointer<MassOptions> options;
    SignalMonitor* monitor;
    GeneralOptions* genOptions;

    bool available;
};

#endif // MASSWINDOW_H
