#ifndef SOLVER_H
#define SOLVER_H

#include <QObject>
#include <QVector>
#include <Eigen/Dense>

class MassCalibration;

double limit(double val, double min, double max);

/**
 * @brief Finds the mass on each cell, based on the observed changes on the FBG peaks.
 */
class Solver : public QObject
{
    Q_OBJECT
public:
    explicit Solver(QObject *parent = 0);

    void prepare(const QVector<double> &reference, const QVector<double>& current, int time,
                 const MassCalibration* calibration, const QVector<double>& params);

    int getTime() const;

    static QVector<double> getDefaultParams();
    static QVector<QString> getParamLabels();

public slots:
    /**
     * Starts the computation. Emits finished(x) when the computation ends.
     */
    QVector<double> solve();

signals:
    /**
     * Called when the computation is finished. x will be empty in case of errors.
     * Time represents the time passed to the prepare() method.
     */
    void finished(const QVector<double>& x, int time) const;

private:
    Q_DISABLE_COPY(Solver)

    // actual solve()
    static QVector<double> solve_it(const Eigen::MatrixXd& A, const QVector<double>& values, const QVector<double> &p);

    // short-lived state set by prepare()
    QVector<double> values;
    QVector<double> params;
    Eigen::MatrixXd prepA;

    const MassCalibration* calibration;
    int time;

    bool prepared;
};

#endif // SOLVER_H
