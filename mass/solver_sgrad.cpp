#include "config.h"

#ifdef WITH_SGRAD

#include "solver.h"
#include "generaloptions.h"

static Eigen::MatrixXd pinv(const Eigen::MatrixXd& a,
                     double epsilon = std::numeric_limits<double>::epsilon())
{
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(a, Eigen::ComputeThinU | Eigen::ComputeThinV);
    double tolerance = epsilon * std::max(a.cols(), a.rows()) * svd.singularValues().array().abs()(0);

    return svd.matrixV() * (svd.singularValues().array().abs() > tolerance)
           .select(svd.singularValues().array().inverse(), 0).matrix().asDiagonal() * svd.matrixU().adjoint();
}

QVector<double> Solver::solve_it(const Eigen::MatrixXd& A, const QVector<double>& values)
{
    // subgradient algorithm parameters
    const double alpha_0   = 0.1;
    const double max_it    = 1e5;
    const double max_stuck = 5e3;

    // convert to Vector
    const int size = values.size();
    Eigen::VectorXd y(size);

    for(int i = 0; i < size; ++i)
        y[i] = values[i];

    // helpers
    const Eigen::MatrixXd A_pinv  = pinv(A);
    const Eigen::MatrixXd I       = Eigen::MatrixXd::Identity(A.cols(), A.cols());
    const Eigen::MatrixXd P = I - A_pinv * A;   // projection

    Eigen::VectorXd x = A_pinv * y;             // initial solution
    Eigen::VectorXd g = Eigen::VectorXd::Zero(A.cols(), 1);

    Eigen::VectorXd best_x = x;

    double best_l1 = 1e99;
    unsigned stuck = 0;

    for(unsigned it = 0; it < max_it; ++it)
    {
        // compute the gradient
        for(int i = 0, e = g.rows(); i < e; ++i)
            g[i] = x[i] >= 0 ? 1 : -1;

        // step
        const double alpha = alpha_0 / g.norm();
        x -= alpha * P * g;

        // stoping condition
        double l1 = 0;

        for(int i = 0, e = x.rows(); i < e; ++i)
            l1 += std::abs(x[i]);

        // track the best
        if(l1 < best_l1)
        {
            best_x  = x;
            best_l1 = l1;
        }
        else if(++stuck > max_stuck) break;
    };

    // convert to output
    QVector<double> out(best_x.rows());

    for(int i = 0, e = x.rows(); i < e; ++i)
        out[i] = std::max(0.0, best_x[i]);

    return out;
}

SolverParams Solver::getSolverParams()
{
    return SolverParams {0.0, 0.0, 0.0, 0.0, 0.0};
}

#endif
