#include "massoptions.h"
#include "ui_massoptions.h"
#include "config.h"
#include "utils.h"

#include <QFileDialog>
#include <QSettings>

MassOptions::MassOptions(QWidget *parent) :
    QGroupBox(parent), side(MASS_SIDE), padding(MASS_PADDING),
    scaleWidth(MASS_SCALE_WIDTH), ui(new Ui::MassOptions)
{
    ui->setupUi(this);
    connect(ui->btnCalibrate, SIGNAL(clicked()), this, SIGNAL(showCalibrationWizard()));
    connect(ui->btnSelectCalibration, SIGNAL(clicked()), this, SLOT(selectCalibrationPath()));
}

MassOptions::~MassOptions()
{
    delete ui;
}

double MassOptions::getScale() const
{
    if(ui->radioNormalized->isChecked())
        return 0.0;
    else
        return ui->spinManual->value();
}

void MassOptions::setScale(double value)
{
    if(value == 0.0)
    {
        ui->radioNormalized->setChecked(true);
    }
    else
    {
        ui->radioManual->setChecked(true);
        ui->spinManual->setValue(value);
    }
}

MassDimension MassOptions::getDimension() const
{
    if(ui->radioNone->isChecked())
        return DimensionNone;

    if(ui->radioGram->isChecked())
        return DimensionGram;

    return DimensionKilogram;
}

void MassOptions::setDimension(MassDimension value)
{
    switch(value)
    {
        case DimensionNone:
            ui->radioNone->setChecked(true);
            break;

        case DimensionGram:
            ui->radioGram->setChecked(true);
            break;

        default:
            ui->radioKilogram->setChecked(true);
            break;
    }
}

bool MassOptions::getRecordData() const
{
    return ui->checkRecord->isChecked();
}

void MassOptions::setRecordData(bool value)
{
    ui->checkRecord->setChecked(value);
}

QString MassOptions::getCalibrationPath() const
{
    if(calibrationPath.isEmpty())
        return getStorageLocation() + "/calibration.txt";
    else
        return calibrationPath;
}

void MassOptions::setCalibrationPath(const QString &value)
{
    calibrationPath = value;
}

void MassOptions::loadSettings(QSettings &st)
{
    setScale(st.value("massScale").toDouble());
    setDimension((MassDimension) st.value("massDimension").toInt());
    setRecordData(st.value("massRecordData").toBool());
    setCalibrationPath(st.value("massCalibrationPath").toString());
}

void MassOptions::saveSettings(QSettings &st) const
{
    st.setValue("massScale", getScale());
    st.setValue("massDimension", getDimension());
    st.setValue("massRecordData", getRecordData());
    st.setValue("massCalibrationPath", getCalibrationPath());
}

void MassOptions::selectCalibrationPath()
{
    QString newPath = QFileDialog::getOpenFileName(this,
                      tr("Select calibration file"), calibrationPath);

    setCalibrationPath(newPath);
}
int MassOptions::getScaleWidth() const
{
    return scaleWidth;
}

void MassOptions::setScaleWidth(int value)
{
    scaleWidth = value;
}

int MassOptions::getPadding() const
{
    return padding;
}

void MassOptions::setPadding(int value)
{
    padding = value;
}

int MassOptions::getSide() const
{
    return side;
}

void MassOptions::setSide(int value)
{
    side = value;
}

