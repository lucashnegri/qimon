#include "masscalibrationwizard.h"
#include "ui_masscalibrationwizard.h"
#include "masscalibration.h"
#include "signalmonitor.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include <QTime>
#include <QTimer>
#include <QDir>
#include <iostream>
#include <iomanip>

enum WizardPageId
{
    PageLoadCreate = 0,
    PageAcquisition
};

MassCalibrationWizard::MassCalibrationWizard(QWidget *parent) :
    QWizard(parent), ui(new Ui::MassCalibrationWizard), timer(new QTimer(this))
{
    ui->setupUi(this);
    connect(ui->btnAcquire  , SIGNAL(clicked())        , this, SLOT(acquirePosition()));
    connect(ui->btnClear    , SIGNAL(clicked())        , this, SLOT(clearPosition()));
    connect(ui->spinPosition, SIGNAL(valueChanged(int)), this, SLOT(positionChanged(int)));
}

MassCalibrationWizard::~MassCalibrationWizard()
{
    delete ui;
}

void MassCalibrationWizard::runWizard(SignalMonitor *monitor)
{
    monitor->stopAcquisition();
    this->monitor = monitor;
    restart();
    show();
}

// called before the page is shown
void MassCalibrationWizard::initializePage(int id)
{
    if(id == PageAcquisition)
    {
        const int positions = calibration->getPositions();
        ui->spinPosition->setMaximum(positions);
        ui->spinPosition->setValue(0); positionChanged(0); // 0 = reference
        ui->plainLog->clear();

        avg = QVector<MovingAverage>(positions + 1);

        const QString msg = tr("Initialization: %1 rows, %2 cols, and %3 sensors")
                            .arg(calibration->getRows())
                            .arg(calibration->getCols())
                            .arg(calibration->getSensors());
        log(msg);
    }
}

// called when clicking next / finish
bool MassCalibrationWizard::validateCurrentPage()
{
    switch(currentId())
    {
        case PageLoadCreate:
            return validateLoadCreatePage();

        case PageAcquisition:
            return validateAcquisitionPage();

        default:
            return true;
    }
}

bool MassCalibrationWizard::validateLoadCreatePage()
{
    const int rows    = ui->spinRows->value();
    const int cols    = ui->spinCols->value();
    const int sensors = ui->spinSensors->value();
    calibration.reset(new MassCalibration(rows, cols, sensors));
    return true;
}

bool MassCalibrationWizard::validateAcquisitionPage()
{
    disableTimer();
    const QString path = QFileDialog::getSaveFileName(this, tr("Save calibration file"),
                         calibrationPath);

    if(path.isEmpty())
        return false; // user canceled the action

    try
    {
        computeCalibration();
        calibration->save(path);
        return true;
    }
    catch(const std::exception& exp)
    {
        QString msg = tr("Could not save calibration file:\n\n");
        msg += exp.what();
        QMessageBox::warning(this, tr("Warning"), msg);
        return false;
    }
}

void MassCalibrationWizard::done(int result)
{
    disableTimer();
    QWizard::done(result);
}

void MassCalibrationWizard::reject()
{
    auto resp = QMessageBox::question(this, tr("Confirmation"),
                                      "Cancel the calibration procedure?",
                                      QMessageBox::No | QMessageBox::Yes,
                                      QMessageBox::No);

    if(resp == QMessageBox::Yes)
    {
        disableTimer();
        QWizard::reject();
    }
}

void MassCalibrationWizard::setPath(const QString &path)
{
    this->calibrationPath = path;
}

void MassCalibrationWizard::enableTimer()
{
    timer->setSingleShot(true);
    const int msec = ui->spinWaiting->value() * 1000;
    timer->start(msec);
    ui->acquisitionPage->setEnabled(false);
}

void MassCalibrationWizard::disableTimer()
{
    timer->stop();
    timer->disconnect(SIGNAL(timeout()));
    ui->acquisitionPage->setEnabled(true);
}

void MassCalibrationWizard::log(const QString &msg)
{
    const QString time = QTime::currentTime().toString();

    ui->plainLog->moveCursor(QTextCursor::End);
    ui->plainLog->insertPlainText("[" + time + "] ");
    ui->plainLog->insertPlainText(msg);
    ui->plainLog->insertPlainText("\n");
    ui->plainLog->ensureCursorVisible();
}

void MassCalibrationWizard::guide()
{
    if(!ui->checkGuided->isChecked()) return;

    const int current = ui->spinPosition->value();
    const int total   = calibration->getPositions();
    const int next = current < total ? current + 1 : 1;
    ui->spinPosition->setValue(next);
}

// acquisition

void MassCalibrationWizard::acquirePosition()
{
    connect(timer, SIGNAL(timeout()), this, SLOT(doAcquire()));
    enableTimer();
}

void MassCalibrationWizard::doAcquire()
{
    disableTimer();

    try
    {
        const int repetitions = ui->spinRepetitions->value();
        const int position    = ui->spinPosition->value();
        const int sensors     = calibration->getSensors();

        const QVector<double>& peaks = monitor->getAverage(repetitions);

        if(sensors != peaks.size())
            throw tr("Mismatch in the number of peaks");

        avg[position].update(peaks);

        if(position == 0)
            log(tr("Acquired reference signal"));
        else
            log(tr("Acquired signal at position %1").arg(position));

        guide();
    }
    catch(const QString& str)
    {
        log(str);
    }
    catch(const std::exception&)
    {
        log(tr("Load acquisition failed"));
    }
}

void MassCalibrationWizard::positionChanged(int v)
{
    QString suffix = tr(" of ") + QString::number(calibration->getPositions());

    if(v == 0)
        suffix += " (reference)";

    ui->spinPosition->setSuffix(suffix);
}

void MassCalibrationWizard::clearPosition()
{
    const int position = ui->spinPosition->value();
    avg[position].reset();

    if(position == 0)
        log(tr("Cleared reference signals"));
    else
        log(tr("Cleared signals at position %1").arg(position));
}

void MassCalibrationWizard::computeCalibration()
{
    const double mass_kg = ui->spinMass->value() / 1000.0;
    const int    sensors = calibration->getSensors();

    // get the reference and check if it is valid
    const auto ref = avg[0].getAverage();

    if(ref.size() != sensors)
        throw std::runtime_error("Mismatch in the number of reference peaks");

    std::cout << std::fixed << std::setprecision(4);

    for(double r : ref)
    {
        if(r < 1.0)
            throw std::runtime_error("Invalid reference peaks");

        std::cout << r << " ";
    }
    std::cout << std::endl;

    // start the calibration procedure
    auto& mtx = calibration->getMatrix();

    for(int pos = 1, e = calibration->getPositions(); pos <= e; ++pos)
    {
        const auto peaks = avg[pos].getAverage();

        if(peaks.size() != ref.size())
            throw std::runtime_error("Mismatch between the reference and position acquisitions");

        // fill the column for the selected position (zero-based)
        for(int i = 0; i < sensors; ++i)
            mtx(i, pos - 1) = (peaks[i] - ref[i]) / mass_kg;
    }

    monitor->setReference(ref);
}
