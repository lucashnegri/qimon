#include "signalmonitor.h"
#include "config.h"
#include "acquisitionoptions.h"
#include "imon.h"
#include "movingaverage.h"

#include <QMessageBox>

SignalMonitor::SignalMonitor(AcquisitionOptions *options_, QObject *parent) :
    QObject(parent), data(IMON_N_WAVELENGHTS),
    refAvg(new MovingAverage()), imon(new IMON()),
    options(options_), working(false), singleShoot(false)
{
    connect(&repeatTimer, SIGNAL(timeout()), this, SLOT(acquire()));
    stopwatch.start();
}

SignalMonitor::~SignalMonitor()
{
    delete refAvg;
    delete imon;
}

bool SignalMonitor::isWorking() const
{
    return working;
}

const QVector<double> &SignalMonitor::getCurrent()
{
    return current;
}

QVector<double> SignalMonitor::getAverage(int rep)
{
    MovingAverage avg(rep);

    for(int i = 0; i < rep; ++i)
    {
        singleAcquisition(true);
        avg.update(current);
    }

    return avg.getAverage();
}

void SignalMonitor::singleAcquisition(bool now)
{
    singleShoot = true;
    startAcquisition();

    if(now) acquire();
}

void SignalMonitor::startAcquisition()
{
    QByteArray arr = options->getDeviceName().toLocal8Bit();

    imon->initialize(options->getCycleTime() * 1e3,
                     options->getExposureTime() * 1e3,
                     arr.constData());
    setWorking(true);
    repeatTimer.start(options->getTotalTime());
}

void SignalMonitor::stopAcquisition()
{
    if(!working)
        return;

    repeatTimer.stop();
    setWorking(false);
    imon->finalize();
}

void SignalMonitor::acquire()
{
    // can't throw, we are inside a slot!
    try
    {
        /* acquire data (raw) */
        imon->acquire(data);
        const double time = stopwatch.elapsed();
        emit signalAcquired(data);

        if(options->getBaselineIters() > 0)
            data.removeBaseline(options->getBaselineOrder(), options->getBaselineIters());

        double min, max;
        options->getRange(min, max);
        data.setRange(min, max);

        emit signalPrepared(data);

        /* scale */
        data.calcBounds();
        data.scaleX(ScaleInfo(0.0, 1.0));
        data.scaleY(ScaleInfo(0.0, 1.0));

        current = data.detectPeaks(options->getPeakRising(), options->getPeakFalling(),
                                   options->getPeakPoints());

        /* use only the selected FBGs */
        QVector<int> sel_idx = options->getSelectedFBGsVector(current.size());
        QVector<double> sel_peaks;

        foreach(int idx, sel_idx) {
            sel_peaks.append(current[idx]);
        }

        current = sel_peaks;

        emit peaksComputed(getReference(), current, time);

        if(singleShoot)
        {
            singleShoot = false;
            stopAcquisition();
        }
    }
    catch(const std::exception& e)
    {
        try
        {
            stopAcquisition();
        } catch(...)
        {
            // silent catch
        }

        QMessageBox::critical(0, tr("Exception"), QString(e.what()));
    }
}

void SignalMonitor::setWorking(bool working)
{
    if(working)
        stopwatch.restart();

    this->working = working;
    emit workingChanged(working);
}

void SignalMonitor::addReference()
{
    refAvg->update(current);
}

const QVector<double> &SignalMonitor::getReference()
{
    return refAvg->getAverage();
}

void SignalMonitor::setReference(const QVector<double> &newReference)
{
    clearReference();
    refAvg->update(newReference);
}

void SignalMonitor::clearReference()
{
    refAvg->reset();
}
