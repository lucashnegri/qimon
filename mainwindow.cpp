#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "data.h"
#include "signalmonitor.h"
#include "acquisitionoptions.h"
#include "generaloptions.h"
#include "logger.h"

#ifdef WITH_MASSVIEW
#include "mass/masswindow.h"
#endif

#include <QFileDialog>
#include <QPushButton>
#include <QMessageBox>
#include <QDate>
#include <QtGlobal>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), avg(MOV_AVG_SAMPLES), acqOptions(new AcquisitionOptions()),
      monitor(new SignalMonitor(acqOptions, this)), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->plot->configurePlot(tr("Wavelength [nm]"), tr("Count"), true);
    const QDate build_date = QLocale(QLocale::C).toDate(QString(__DATE__).simplified(),
                             QLatin1String("MMM d yyyy"));
    ui->labelStatus->setText(QString("QIMON ") + build_date.toString("yyyy.MM.dd") +
                             QString(" - Lucas Hermann Negri"));

    // setup options page
    genOptions = new GeneralOptions(ui->lineName);
    ui->optionsLayout->addWidget(genOptions);
    ui->optionsLayout->addWidget(acqOptions);
#ifdef WITH_MASSVIEW
    setupMassView();
#endif
    ui->optionsLayout->addStretch(1);

    // load settings
    QSettings settings(QApplication::organizationName(), QApplication::applicationName());
    loadSettings(settings);

    // setup the signal acquisition
    logger = new Logger(genOptions, this);
    setupAcquisition();
}

void MainWindow::setupAcquisition()
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 2, 0))
    ui->lineName->setClearButtonEnabled(true);
#endif

    connect(ui->btnToday , SIGNAL(clicked()), this, SLOT(setToday()));
    connect(ui->btnSingle, SIGNAL(clicked()), this, SLOT(singleAcquisition()));
    connect(ui->btnStart , SIGNAL(clicked()), this, SLOT(startRepeatedAcquisition()));
    connect(ui->btnStop  , SIGNAL(clicked()), this, SLOT(stopRepeatedAcquisition()));

    connect(ui->btnAdd   , SIGNAL(clicked()), monitor, SLOT(addReference()));
    connect(ui->btnClear , SIGNAL(clicked()), monitor, SLOT(clearReference()));

    connect(monitor, SIGNAL(signalPrepared(const Data&)),   this, SLOT(signalPrepared(const Data&)));
    connect(monitor, SIGNAL(peaksComputed(const QVector<double>&, const QVector<double>&, int)),
            this   , SLOT(peaksComputed(const QVector<double>&, const QVector<double>&, int)));

    connect(monitor, SIGNAL(signalAcquired(const Data&)), logger, SLOT(appendRawData(const Data&)));
    connect(monitor, SIGNAL(peaksComputed(const QVector<double>&, const QVector<double>&, int)),
            logger , SLOT(appendPeaks(QVector<double>, QVector<double>, int)));

    connect(monitor, SIGNAL(workingChanged(bool)), this, SLOT(workingChanged(bool)));
}

#ifdef WITH_MASSVIEW
void MainWindow::setupMassView()
{
    massWindow = new MassWindow(monitor, genOptions, this);
    massWindow->setWindowFlags(Qt::Window);

    QPushButton* btn = new QPushButton(tr("&Mass"), this);
    connect(btn, SIGNAL(clicked()), massWindow, SLOT(toggle()));
    ui->toolLayout->addWidget(btn);

    connect(monitor   , SIGNAL(peaksComputed(QVector<double>, QVector<double>, int)),
            massWindow, SLOT(peaksComputed(QVector<double>, QVector<double>, int)));

    ui->optionsLayout->addWidget(massWindow->getOptionsWidget());
}
#endif

MainWindow::~MainWindow()
{
    QSettings settings(QApplication::organizationName(), QApplication::applicationName());
    saveSettings(settings);

    delete ui;
}

// restore the configuration from the last session
void MainWindow::loadSettings(QSettings &st)
{
    restoreGeometry(st.value("windowGeometry").toByteArray());
    restoreState(st.value("windowState").toByteArray());
    acqOptions->loadSettings(st);
    genOptions->loadSettings(st);

#ifdef WITH_MASSVIEW
    massWindow->loadSettings(st);
#endif
}

// save the current configuration for the next session
void MainWindow::saveSettings(QSettings &st) const
{
    st.setValue("windowGeometry"     , saveGeometry());
    st.setValue("windowState"        , saveState());
    acqOptions->saveSettings(st);
    genOptions->saveSettings(st);

#ifdef WITH_MASSVIEW
    massWindow->saveSettings(st);
#endif
}

void MainWindow::displaySignalInfo(const QVector<double>& reference,
                                   const QVector<double>& current)
{
    QString msg("Peaks (");
    msg += QString::number(current.size());
    msg += QString("): ");

    double max_delta = 0.0;

    for(int i = 0, e = current.size(); i < e; ++i)
    {
        double diff = current[i];

        if(reference.size() >= current.size())
        {
            diff -= reference[i];
        }

        double abs_diff = fabs(diff);
        if(abs_diff > max_delta)
            max_delta = abs_diff;

        msg += QString(" ") + QString::number(diff, 'f', 3);
    }

    msg += QString(" (") + QString::number(max_delta, 'f', 3) + QString(")");

    ui->labelStatus->setText(msg);
}

void MainWindow::workingChanged(bool working)
{
    // UI
    genOptions->setRecordingOptionsEnabled(!working);
    ui->lineName->setEnabled(!working);
    ui->btnSingle->setEnabled(!working);
    ui->btnStart->setEnabled(!working);
    ui->btnStop->setEnabled(working);
    ui->btnToday->setEnabled(!working);

    // logger
    if(working)
    {
        if(!logger->prepare())
            QMessageBox::warning(this, tr("Warning"), tr("Could not open files for recording."));
    }
    else
    {
        logger->finish();
    }

    avg.reset();
}

void MainWindow::startRepeatedAcquisition()
{
    try
    {
        monitor->startAcquisition();
    }
    catch(const std::exception& e)
    {
        QMessageBox::critical(this, tr("Exception"), QString(e.what()));
    }
}

void MainWindow::stopRepeatedAcquisition()
{
    try
    {
        monitor->stopAcquisition();
    }
    catch(const std::exception& e)
    {
        QMessageBox::critical(this, tr("Exception"), QString(e.what()));
    }
}

void MainWindow::singleAcquisition()
{
    try
    {
        monitor->singleAcquisition();
    }
    catch(const std::exception& e)
    {
        QMessageBox::critical(this, tr("Exception"), QString(e.what()));
    }
}

void MainWindow::setToday()
{
    ui->lineName->setText(QDate::currentDate().toString("yyyy_MM_dd"));
}

void MainWindow::signalPrepared(const Data &prepared)
{
    ui->plot->doPlot(prepared.x, prepared.y);
}

void MainWindow::peaksComputed(const QVector<double> &reference, const QVector<double> &current,
                               int time)
{
    Q_UNUSED(time)
    displaySignalInfo(reference, ui->checkSmooth->isChecked() ? avg.update(current) : current);
}
