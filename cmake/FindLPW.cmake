set(LPW_FOUND FALSE)
set(LPW_LIBRARY_NAME lpw)
set(LPW_INCLUDE_DIRS /usr/include /usr/local/include)

find_path(LPW_INCLUDE_DIR lpw ${LPW_INCLUDE_DIRS} )
find_library(LPW_LIBRARY NAMES ${LPW_LIBRARY_NAME} PATHS /usr/lib /usr/local/lib)

if(LPW_INCLUDE_DIR AND LPW_LIBRARY)
  set(LPW_FOUND TRUE)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(lpw DEFAULT_MSG LPW_LIBRARY LPW_INCLUDE_DIR)
mark_as_advanced(LPW_INCLUDE_DIR LPW_LIBRARY)
