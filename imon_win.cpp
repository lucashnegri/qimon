#include "config.h"
#include <QtGlobal>

#ifndef WITH_MOCK_IMON
#ifndef Q_OS_LINUX

#include <QThread>
#include <stdexcept>
#include <HSSUSB2.h>
#include "data.h"
#include "imon.h"

static const long IMON_MODE_COUNT = 0x00000000;
static const long IMON_DATA_COUNT = 1;
static const long IMON_TRANSMIT_COUNT = 1;
static const long IMON_FRAME_COUNT = 1;
static const long IMON_HEADER_SIZE = 256;
static const long IMON_MAX_TRIES = 2000;
static const long IMON_SLEEP_MS = 1;

IMON::IMON() : initialized(false), device_id(0)
{
}

IMON::~IMON()
{
    if(initialized)
        finalize();
}

// may throw std::runtime_error
void IMON::initialize(double exposureCycle, double exposureTime, const char* deviceName)
{
    Q_UNUSED(deviceName);

    if(initialized)
        finalize();

    long ret = USB2_initialize();

    if(ret != usb2Success)
        throw std::runtime_error("Dll coult not be initialized");

    try
    {
        short devicesID[8];
        unsigned short usNum = 0;

        ret = USB2_getModuleConnectionList(devicesID, &usNum);

        if(ret != usb2Success)
            throw std::runtime_error("Module connection failed");

        device_id = devicesID[0];
        ret = USB2_open(device_id);

        if(ret != usb2Success)
            throw std::runtime_error("Device could not be openned");

        initialized = true;

        ret = USB2_setCaptureMode(device_id, IMON_MODE_COUNT);

        if(ret != usb2Success)
            throw std::runtime_error("USB2_setCaptureMode failed");

        ret = USB2_setDataCount(device_id, IMON_DATA_COUNT);

        if(ret != usb2Success)
            throw std::runtime_error("USB2_setDataCount failed");

        ret = USB2_setDataTransmit(device_id, IMON_TRANSMIT_COUNT);

        if(ret != usb2Success)
            throw std::runtime_error("USB2_setDataTransmit failed");

        ret = USB2_setExposureCycle(device_id, exposureCycle);

        if(ret != usb2Success)
            throw std::runtime_error("USB2_setExposureCycle failed");

        ret = USB2_setExposureTime(device_id, exposureTime);

        if(ret != usb2Success)
            throw std::runtime_error("USB2_setExposureTime failed");

        long size_x = 0, size_y = 0;
        ret = USB2_getImageSize(device_id, &size_x, &size_y);

        if(ret != usb2Success)
            throw std::runtime_error("USB2_getImageSize failed");

        ret = USB2_allocateBuffer(device_id, IMON_FRAME_COUNT);

        if(ret != usb2Success)
            throw std::runtime_error("USB2_allocateBuffer failed");
    }
    catch(const std::exception&)
    {
        finalize();
        throw;
    }
}

// may throw std::runtime_error
void IMON::acquire(Data &data)
{
    long ret = USB2_captureStart(device_id, IMON_FRAME_COUNT);

    if(ret != usb2Success)
        throw std::runtime_error("Failed to start the data capture");

    long capture_frame_count = 0, current_capture_index = 0;

    int tries;

    for(tries = 0; tries < IMON_MAX_TRIES; ++tries)
    {
        ret = USB2_getCaptureStatus(device_id, &capture_frame_count, &current_capture_index);

        if(ret == usb2Success)
            break;

        QThread::msleep(IMON_SLEEP_MS);
    }

    if(tries == IMON_MAX_TRIES)
        throw std::runtime_error("USB2_getCaptureStatus failed");

    long old_index = 0;

    if(capture_frame_count > current_capture_index + 1)
        old_index = IMON_FRAME_COUNT - (capture_frame_count - current_capture_index + 1);
    else
        old_index = current_capture_index - capture_frame_count + 1;

    int SIZE = IMON_HEADER_SIZE * capture_frame_count;
    std::vector<unsigned short> image_header(SIZE, 0);

    long image_data_size = capture_frame_count * 512;
    std::vector<unsigned short> image_data(image_data_size, 0);

    ret = USB2_getImageHeaderData(device_id, &image_header[0], &image_data[0], old_index,
                                  capture_frame_count);

    if(ret != usb2Success)
        throw std::runtime_error("USB2_getImageHeaderData failed");

    data.checkSize(image_data_size);

    for(int i = 0; i < image_data_size; ++i)
    {
        data.x[i] = IMON_WAVELENGTHS[i] / 1000.0;
        data.y[image_data_size - i - 1] = image_data[i]; // reverse order
    }

    ret = USB2_captureStop(device_id);

    if(ret != usb2Success)
        throw std::runtime_error("USB2_captureStop failed");
}

void IMON::finalize()
{
    initialized = false;

    USB2_releaseBuffer(device_id);
    USB2_close(device_id);
    USB2_uninitialize();
}

#endif

#endif // WITH_MOCK_IMON
