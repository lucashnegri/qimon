/*
 * It is a small library for work with imon512e2 driver;
 * functions of this library are simple example, how you
 * can to get data from I-MON E-USB 2.0 spectrometer by
 * imon512e2 driver.
 *
 * Copyright (C) 2009 Alexander Lyasin
 * <alexander.lyasin@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation or under the terms of the
 * BSD License
 */
/*
 * libimon2.h
 */

#include <QtGlobal>
#ifdef Q_OS_LINUX

#ifndef LIBIMON2_H
#define LIBIMON2_H

#include <asm/ioctl.h>
#include <asm/types.h>

#define GET_MODULE_INFORMATION 0x01
#define GET_SPECTRO_INFORMATION 0x02
#define GET_MODULE_PROPERTY 0x03
#define GET_DATAPOSITION_PROPERTY 0x04
#define GET_DATACOUNT_PROPERTY 0x05
#define GET_DATATRANSMIT_PROPERTY 0x06
#define GET_DATATRIGGEROFFSET_PROPERTY 0x07
#define GET_EXPOSURE_PROPERTY 0x08
#define GET_GAIN_PROPERTY 0x09
#define GET_ADOFFSET_PROPERTY 0x0A
#define GET_COOLING_PROPERTY 0x0B
#define GET_WAVELENGTH_PROPERTY 0x0C
#define GET_IMAGE_SIZE 0x0D
#define GET_MODULEREADOUT_TIME 0x0E
#define GET_CAPTURE_MODE 0x0F
#define SET_CAPTURE_MODE 0x10
#define GET_DATA_POSITION 0x11
#define SET_DATA_POSITION 0x12
#define GET_DATA_COUNT 0x13
#define SET_DATA_COUNT 0x14
#define GET_DATA_TRANSMIT 0x15
#define SET_DATA_TRANSMIT 0x16
#define GET_DATATRIGGER_OFFSET 0x17
#define SET_DATATRIGGER_OFFSET 0x18
#define GET_EXPOSURE_TIME 0x19
#define SET_EXPOSURE_TIME 0x1A
#define GET_EXPOSURE_CYCLE 0x1B
#define SET_EXPOSURE_CYCLE 0x1C
#define GET_TRIGGER_MODE 0x1D /* begin supposed methods */
#define SET_TRIGGER_MODE 0x1E
#define GET_TRIGGER_POLARITY 0x1F
#define SET_TRIGGER_POLARITY 0x20
#define GET_TRIGGER_OUTPUT 0x21
#define SET_TRIGGER_OUTPUT 0x22
#define GET_BINNING_SUBARRAY 0x23                                 /* begin unimplemented methods */
#define SET_BINNING_SUBARRAY 0x24 /* end unimlpemented methods */ /* end supposed methods */
#define GET_GAIN 0x25
#define SET_GAIN 0x26
#define GET_ADOFFSET 0x27 /* begin supposed methods */
#define SET_ADOFFSET 0x28
#define GET_CALIBRATION_COEFFICIENT 0x29
#define GET_COOLING_TEMPERATURE 0x2A
#define SET_COOLING_TEMPERATURE 0x2B
#define GET_COOLING_STATUS 0x2C
#define SET_COOLING_STATUS 0x2D
#define GET_CURRENT_TEMPERATURE 0x2E /* end supposed methods */
#define CAPTURE_START 0x2F
#define GET_CAPTURE_STATUS 0x30
#define CAPTURE_STOP 0x31

#define GET_BEGCYCLE_TRANSFER 0x41
#define GET_ENDCYCLE_TRANSFER 0x42
#define GET_ENDCAPTURE_TRANSFER 0x43
#define GET_ENDHEADER_TRANSFER 0x44

#define RCV_BEGINCYCLE_PKT                                                                         \
    {                                                                                              \
        0x02, 0x80                                                                                 \
    }
#define RCV_ENDCYCLE_PKT                                                                           \
    {                                                                                              \
        0x04, 0x80, 0x00, 0x00, 0x00, 0x00                                                         \
    }
#define RCV_ENDCAPTURE_PKT                                                                         \
    {                                                                                              \
        0x01, 0x80, 0x00, 0x00, 0x00, 0x00                                                         \
    }
#define RCV_HEADER_SIZE 0x200
#define RCV_IMAGE_SIZE 0x400

#define IMON512E2_MAGIC_IOCTL 0xbd
/*
 * ioctl commands
 */
#define IO_GET_MODULE_INFORMATION _IOR(IMON512E2_MAGIC_IOCTL, GET_MODULE_INFORMATION, void *)
#define IO_GET_SPECTRO_INFORMATION _IOR(IMON512E2_MAGIC_IOCTL, GET_SPECTRO_INFORMATION, void *)
#define IO_GET_MODULE_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_MODULE_PROPERTY, void *)
#define IO_GET_DATAPOSITION_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_DATAPOSITION_PROPERTY, void *)
#define IO_GET_DATACOUNT_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_DATACOUNT_PROPERTY, __s32)
#define IO_GET_DATATRANSMIT_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_DATATRANSMIT_PROPERTY, __s32)
#define IO_GET_DATATRIGGEROFFSET_PROPERTY                                                          \
    _IOR(IMON512E2_MAGIC_IOCTL, GET_DATATRIGGEROFFSET_PROPERTY, void *)
#define IO_GET_EXPOSURE_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_EXPOSURE_PROPERTY, void *)
#define IO_GET_GAIN_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_GAIN_PROPERTY, __s32)
#define IO_GET_ADOFFSET_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_ADOFFSET_PROPERTY, void *)
#define IO_GET_COOLING_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_COOLING_PROPERTY, void *)
#define IO_GET_WAVELENGTH_PROPERTY _IOR(IMON512E2_MAGIC_IOCTL, GET_WAVELENGTH_PROPERTY, void *)
#define IO_GET_IMAGE_SIZE _IOR(IMON512E2_MAGIC_IOCTL, GET_IMAGE_SIZE, void *)
#define IO_GET_MODULEREADOUT_TIME _IOR(IMON512E2_MAGIC_IOCTL, GET_MODULEREADOUT_TIME, void *)
#define IO_GET_CAPTURE_MODE _IOR(IMON512E2_MAGIC_IOCTL, GET_CAPTURE_MODE, __s32)
#define IO_SET_CAPTURE_MODE _IOW(IMON512E2_MAGIC_IOCTL, SET_CAPTURE_MODE, __s16)
#define IO_GET_DATA_POSITION _IOR(IMON512E2_MAGIC_IOCTL, GET_DATA_POSITION, __s32)
#define IO_SET_DATA_POSITION _IOW(IMON512E2_MAGIC_IOCTL, SET_DATA_POSITION, __s32)
#define IO_GET_DATA_COUNT _IOR(IMON512E2_MAGIC_IOCTL, GET_DATA_COUNT, __s32)
#define IO_SET_DATA_COUNT _IOW(IMON512E2_MAGIC_IOCTL, SET_DATA_COUNT, __s32)
#define IO_GET_DATA_TRANSMIT _IOR(IMON512E2_MAGIC_IOCTL, GET_DATA_TRANSMIT, __s32)
#define IO_SET_DATA_TRANSMIT _IOW(IMON512E2_MAGIC_IOCTL, SET_DATA_TRANSMIT, __s32)
#define IO_GET_DATATRIGGER_OFFSET _IOR(IMON512E2_MAGIC_IOCTL, GET_DATATRIGGER_OFFSET, __s32)
#define IO_SET_DATATRIGGER_OFFSET _IOW(IMON512E2_MAGIC_IOCTL, SET_DATATRIGGER_OFFSET, __s32)
#define IO_GET_EXPOSURE_TIME _IOR(IMON512E2_MAGIC_IOCTL, GET_EXPOSURE_TIME, __s64)
#define IO_SET_EXPOSURE_TIME _IOW(IMON512E2_MAGIC_IOCTL, SET_EXPOSURE_TIME, __s64)
#define IO_GET_EXPOSURE_CYCLE _IOR(IMON512E2_MAGIC_IOCTL, GET_EXPOSURE_CYCLE, __s64)
#define IO_SET_EXPOSURE_CYCLE _IOW(IMON512E2_MAGIC_IOCTL, SET_EXPOSURE_CYCLE, __s64)
/* begin supposed methods */
#define IO_GET_TRIGGER_MODE _IOR(IMON512E2_MAGIC_IOCTL, GET_TRIGGER_MODE, __s32)
#define IO_SET_TRIGGER_MODE _IOW(IMON512E2_MAGIC_IOCTL, SET_TRIGGER_MODE, __s32)
#define IO_GET_TRIGGER_POLARITY _IOR(IMON512E2_MAGIC_IOCTL, GET_TRIGGER_POLARITY, __s32)
#define IO_SET_TRIGGER_POLARITY _IOW(IMON512E2_MAGIC_IOCTL, SET_TRIGGER_POLARITY, __s32)
#define IO_GET_TRIGGER_OUTPUT _IOR(IMON512E2_MAGIC_IOCTL, GET_TRIGGER_OUTPUT, __s32)
#define IO_SET_TRIGGER_OUTPUT _IOW(IMON512E2_MAGIC_IOCTL, SET_TRIGGER_OUTPUT, __s32)
/* begin unimplemented methods */
#define IO_GET_BINNING_SUBARRAY
#define IO_SET_BINNING_SUBARRAY
/* end unimlpemented methods */
/* end supposed methods */
#define IO_GET_GAIN _IOR(IMON512E2_MAGIC_IOCTL, GET_GAIN, __s32)
#define IO_SET_GAIN _IOW(IMON512E2_MAGIC_IOCTL, SET_GAIN, void *)
/* begin supposed methods */
#define IO_GET_ADOFFSET _IOR(IMON512E2_MAGIC_IOCTL, GET_ADOFFSET, __s32)
#define IO_SET_ADOFFSET _IOW(IMON512E2_MAGIC_IOCTL, SET_ADOFFSET, __s32)
#define IO_GET_CALIBRATION_COEFFICIENT                                                             \
    _IOR(IMON512E2_MAGIC_IOCTL, GET_CALIBRATION_COEFFICIENT, void *)
#define IO_GET_COOLING_TEMPERATURE _IOR(IMON512E2_MAGIC_IOCTL, GET_COOLING_TEMPERATURE, __s64)
#define IO_SET_COOLING_TEMPERATURE _IOW(IMON512E2_MAGIC_IOCTL, SET_COOLING_TEMPERATURE, __s64)
#define IO_GET_COOLING_STATUS _IOR(IMON512E2_MAGIC_IOCTL, GET_COOLING_STATUS, __s32)
#define IO_SET_COOLING_STATUS _IOW(IMON512E2_MAGIC_IOCTL, SET_COOLING_STATUS, __s32)
#define IO_GET_CURRENT_TEMPERATURE _IOR(IMON512E2_MAGIC_IOCTL, GET_CURRENT_TEMPERATURE, __s64)
/* end supposed methods */
#define IO_CAPTURE_START _IOW(IMON512E2_MAGIC_IOCTL, CAPTURE_START, __s16)
#define IO_GET_CAPTURE_STATUS _IOR(IMON512E2_MAGIC_IOCTL, GET_CAPTURE_STATUS, void *)
#define IO_CAPTURE_STOP _IO(IMON512E2_MAGIC_IOCTL, CAPTURE_STOP)
/* additional (own) commands */
#define IO_GET_BEGCYCLE_TRANSFER _IOR(IMON512E2_MAGIC_IOCTL, GET_BEGCYCLE_TRANSFER, void *)
#define IO_GET_ENDCYCLE_TRANSFER _IOR(IMON512E2_MAGIC_IOCTL, GET_ENDCYCLE_TRANSFER, void *)
#define IO_GET_ENDCAPTURE_TRANSFER _IOR(IMON512E2_MAGIC_IOCTL, GET_ENDCAPTURE_TRANSFER, void *)
#define IO_GET_ENDHEADER_TRANSFER _IOR(IMON512E2_MAGIC_IOCTL, GET_ENDHEADER_TRANSFER, void *)
/*
 * end ioctl commands
 */
struct ModuleInformation
{
    char vendor[32];
    char product[32];
    char serial[32];
    char firmware[32];
};

struct SpectroInformation
{
    char unit[8];
    char sensor[16];
};

struct ModuleProperty
{
    __s32 pixel;
    __s32 resolution;
    __s32 capturemode;
    __s32 binningmode;
    __s32 triggermode;
    __s32 triggerpolarity;
};

struct DataPositionProperty
{
    __s32 lowerposition;
    __s32 upperposition;
};

struct DataTriggerOffsetProperty
{
    __s32 lower;
    __s32 upper;
};

struct ExposureProperty
{
    double lowertime;
    double uppertime;
    double timestep;
    double lowercycle;
    double uppercycle;
    double cyclestep;
};

struct AdOffsetProperty
{
    __s32 lower;
    __s32 upper;
};

struct CoolingProperty
{
    double lower;
    double upper;
    double step;
};

struct WavelengthProperty
{
    __s32 lowerwavelength;
    __s32 upperwavelength;
};

struct ImageSize
{
    __s32 sizeX;
    __s32 sizeY;
};

struct CalibrationCoefficient
{
    double A0;
    double B1;
    double B2;
    double B3;
    double B4;
    double B5;
};

#define over64(dst, src)                                                                           \
    (dst)[0] = (src)[7];                                                                           \
    (dst)[1] = (src)[6];                                                                           \
    (dst)[2] = (src)[5];                                                                           \
    (dst)[3] = (src)[4];                                                                           \
    (dst)[4] = (src)[3];                                                                           \
    (dst)[5] = (src)[2];                                                                           \
    (dst)[6] = (src)[1];                                                                           \
    (dst)[7] = (src)[0];

inline int imon2_get_module_information(int dev_fd, struct ModuleInformation *buf)
{
    return ioctl(dev_fd, IO_GET_MODULE_INFORMATION, buf);
}

inline int imon2_get_spectro_information(int dev_fd, struct SpectroInformation *buf)
{
    return ioctl(dev_fd, IO_GET_SPECTRO_INFORMATION, buf);
}

inline int imon2_get_module_property(int dev_fd, struct ModuleProperty *buf)
{
    return ioctl(dev_fd, IO_GET_MODULE_PROPERTY, buf);
}

inline int imon2_get_dataposition_property(int dev_fd, struct DataPositionProperty *buf)
{
    return ioctl(dev_fd, IO_GET_DATAPOSITION_PROPERTY, buf);
}

inline int imon2_get_datacount_property(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_DATACOUNT_PROPERTY, buf);
}

inline int imon2_get_datatransmit_property(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_DATAPOSITION_PROPERTY, buf);
}

inline int imon2_get_datatriggeroffset_property(int dev_fd, struct DataTriggerOffsetProperty *buf)
{
    return ioctl(dev_fd, IO_GET_DATATRIGGEROFFSET_PROPERTY, buf);
}

inline int imon2_get_exposure_property(int dev_fd, struct ExposureProperty *buf)
{
    return ioctl(dev_fd, IO_GET_EXPOSURE_PROPERTY, buf);
}

inline int imon2_get_gain_property(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_GAIN_PROPERTY, buf);
}

inline int imon2_get_adoffset_property(int dev_fd, struct AdOffsetProperty *buf)
{
    return ioctl(dev_fd, IO_GET_ADOFFSET_PROPERTY, buf);
}

inline int imon2_get_cooling_property(int dev_fd, struct CoolingProperty *buf)
{
    return ioctl(dev_fd, IO_GET_COOLING_PROPERTY, buf);
}

inline int imon2_get_wavelength_property(int dev_fd, struct WavelengthProperty *buf)
{
    return ioctl(dev_fd, IO_GET_WAVELENGTH_PROPERTY, buf);
}

inline int imon2_get_imagesize(int dev_fd, struct ImageSize *buf)
{
    return ioctl(dev_fd, IO_GET_IMAGE_SIZE, buf);
}

inline int imon2_get_modulereadout_time(int dev_fd, double *buf)
{
    return ioctl(dev_fd, IO_GET_MODULEREADOUT_TIME, buf);
}

inline int imon2_get_capturemode(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_CAPTURE_MODE, buf);
}

inline int imon2_set_capturemode(int dev_fd, __s16 mode)
{
    return ioctl(dev_fd, IO_SET_CAPTURE_MODE, mode);
}

inline int imon2_get_dataposition(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_DATA_POSITION, buf);
}

inline int imon2_set_dataposition(int dev_fd, __s32 pos)
{
    return ioctl(dev_fd, IO_SET_DATA_POSITION, pos);
}

inline int imon2_get_datacount(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_DATA_COUNT, buf);
}

inline int imon2_set_datacount(int dev_fd, __s32 data)
{
    return ioctl(dev_fd, IO_SET_DATA_COUNT, data);
}

inline int imon2_get_datatransmit(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_DATA_TRANSMIT, buf);
}

inline int imon2_set_datatransmit(int dev_fd, __s32 data)
{
    return ioctl(dev_fd, IO_SET_DATA_TRANSMIT, data);
}

inline int imon2_get_datatrigger_offset(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_DATATRIGGER_OFFSET, buf);
}

inline int imon2_set_datatrigger_offset(int dev_fd, __s32 offset)
{
    return ioctl(dev_fd, IO_SET_DATATRIGGER_OFFSET, offset);
}

inline int imon2_get_exposuretime(int dev_fd, double *buf)
{
    return ioctl(dev_fd, IO_GET_EXPOSURE_TIME, buf);
}

inline int imon2_set_exposuretime(int dev_fd, double time)
{
    return ioctl(dev_fd, IO_SET_EXPOSURE_TIME, &time);
}

inline int imon2_get_exposurecycle(int dev_fd, double *buf)
{
    return ioctl(dev_fd, IO_GET_EXPOSURE_CYCLE, buf);
}

inline int imon2_set_exposurecycle(int dev_fd, double cycle)
{
    return ioctl(dev_fd, IO_SET_EXPOSURE_CYCLE, &cycle);
}

inline int imon2_get_triggermode(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_TRIGGER_MODE, buf);
}

inline int imon2_set_triggermode(int dev_fd, __s32 mode)
{
    return ioctl(dev_fd, IO_SET_TRIGGER_MODE, mode);
}

inline int imon2_get_triggerpolarity(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_TRIGGER_POLARITY, buf);
}

inline int imon2_set_triggerpolarity(int dev_fd, __s32 polarity)
{
    return ioctl(dev_fd, IO_SET_TRIGGER_POLARITY, polarity);
}

inline int imon2_get_triggeroutput(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_TRIGGER_OUTPUT, buf);
}

inline int imon2_set_triggeroutput(int dev_fd, __s32 data)
{
    return ioctl(dev_fd, IO_SET_TRIGGER_OUTPUT, data);
}

inline int imon2_get_gain(int dev_fd, __s32 mode, __s32 *buf)
{
    memcpy(buf, (void *)&mode, sizeof(mode));
    return ioctl(dev_fd, IO_GET_GAIN, buf);
}

inline int imon2_set_gain(int dev_fd, __s32 mode, __s32 gain)
{
    char buf[sizeof(mode) + sizeof(gain)];
    memcpy(buf, &mode, sizeof(mode));
    memcpy(buf + sizeof(mode), &gain, sizeof(gain));
    return ioctl(dev_fd, IO_SET_GAIN, buf);
}

inline int imon2_get_adoffset(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_ADOFFSET, buf);
}

inline int imon2_set_adoffset(int dev_fd, __s32 offset)
{
    return ioctl(dev_fd, IO_SET_ADOFFSET, offset);
}

inline int imon2_get_calibration_coefficient(int dev_fd, __s32 mode,
        struct CalibrationCoefficient *buf)
{
    memcpy(buf, &mode, sizeof(mode));
    return ioctl(dev_fd, IO_GET_CALIBRATION_COEFFICIENT, buf);
}

inline int imon2_get_coolingtemperature(int dev_fd, double *buf)
{
    return ioctl(dev_fd, IO_GET_COOLING_TEMPERATURE, buf);
}

inline int imon2_set_coolingtemperature(int dev_fd, double t)
{
    return ioctl(dev_fd, IO_SET_COOLING_TEMPERATURE, &t);
}

inline int imon2_get_coolingstatus(int dev_fd, __s32 *buf)
{
    return ioctl(dev_fd, IO_GET_COOLING_STATUS, buf);
}

inline int imon2_set_coolingstatus(int dev_fd, __s32 status)
{
    return ioctl(dev_fd, IO_SET_COOLING_STATUS, status);
}

inline int imon2_get_currenttemperature(int dev_fd, double *buf)
{
    return ioctl(dev_fd, IO_GET_CURRENT_TEMPERATURE, buf);
}

inline int imon2_startcapture(int dev_fd, short mode)
{
    return ioctl(dev_fd, IO_CAPTURE_START, mode);
}

inline int imon2_get_capturestatus(int dev_fd, char *buf)
{
    return ioctl(dev_fd, IO_GET_CAPTURE_STATUS, buf);
}

inline int imon2_stopcapture(int dev_fd)
{
    return ioctl(dev_fd, IO_CAPTURE_STOP);
}

inline int imon2_begin_cycle(int dev_fd, char *buf)
{
    return ioctl(dev_fd, IO_GET_BEGCYCLE_TRANSFER, buf);
}

inline int imon2_end_cycle(int dev_fd, char *buf)
{
    return ioctl(dev_fd, IO_GET_ENDCYCLE_TRANSFER, buf);
}

inline int imon2_end_capture(int dev_fd, char *buf)
{
    return ioctl(dev_fd, IO_GET_ENDCYCLE_TRANSFER, buf);
}

inline int imon2_end_header(int dev_fd, char *buf)
{
    return ioctl(dev_fd, IO_GET_ENDHEADER_TRANSFER, buf);
}

#endif /* end IMON2_H */

#endif
