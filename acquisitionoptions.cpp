#include "acquisitionoptions.h"
#include "ui_acquisitionoptions.h"
#include "config.h"

#include <QSettings>
#include <QResizeEvent>

AcquisitionOptions::AcquisitionOptions(QWidget *parent) :
    QGroupBox(parent), ui(new Ui::AcquisitionOptions)
{
    ui->setupUi(this);
    connect(ui->spinPoints, SIGNAL(editingFinished()), this, SLOT(fixOddPoints()));

    setExposureTime(EXPOSURE_TIME_MS);
    setExposureWait(EXPOSURE_WAIT_MS);
    setPeakRising(PEAK_RISING);
    setPeakFalling(PEAK_FALLING);
    setPeakPoints(PEAK_POINTS);
    setBaselineOrder(BASELINE_ORDER);
    setBaselineIters(BASELINE_ITERS);
    setRange(RANGE_MIN, RANGE_MAX);

    connect(ui->spinMin, SIGNAL(valueChanged(double)), this, SLOT(checkRange(double)));
    connect(ui->spinMax, SIGNAL(valueChanged(double)), this, SLOT(checkRange(double)));
}

AcquisitionOptions::~AcquisitionOptions()
{
    delete ui;
}

QString AcquisitionOptions::getDeviceName() const
{
    return ui->lineDeviceName->text();
}

QString AcquisitionOptions::getSelectedFBGs() const
{
    return ui->lineFBGs->text();
}

void AcquisitionOptions::fixOddPoints()
{
    int val = ui->spinPoints->value();

    if(val % 2 == 0)
        ui->spinPoints->setValue(val - 1);
}

void AcquisitionOptions::resizeEvent(QResizeEvent* e)
{
    // only realign if the width changed
    if(e->size().width() != e->oldSize().width())
    {
        int max_width = 0;
        QList<QLabel*> labels = findChildren<QLabel*>();

        for(int i = 0, e = labels.size(); i < e; ++i)
            max_width = qMax(max_width, labels[i]->width());

        for(int i = 0, e = labels.size(); i < e; ++i)
               labels[i]->setMinimumWidth(max_width);
    }

    ui->rangeLabel->setMinimumWidth(0);
}

double AcquisitionOptions::getPeakRising() const
{
    return ui->spinRising->value();
}

void AcquisitionOptions::setPeakRising(double value)
{
    ui->spinRising->setValue(value);
}

double AcquisitionOptions::getPeakFalling() const
{
    return ui->spinFalling->value();
}

void AcquisitionOptions::setPeakFalling(double value)
{
    ui->spinFalling->setValue(value);
}

double AcquisitionOptions::getExposureTime() const
{
    return ui->spinExposure->value();
}

void AcquisitionOptions::setExposureTime(double value)
{
    ui->spinExposure->setValue(value);
}

double AcquisitionOptions::getExposureWait() const
{
    return ui->spinWait->value();
}

double AcquisitionOptions::getCycleTime() const
{
    return getExposureTime() + CYCLE_WAIT_MS;
}

double AcquisitionOptions::getTotalTime() const
{
    return getCycleTime() + getExposureWait();
}

void AcquisitionOptions::setExposureWait(double value)
{
    ui->spinWait->setValue(value);
}

int AcquisitionOptions::getPeakPoints() const
{
    return ui->spinPoints->value();
}

void AcquisitionOptions::setPeakPoints(int value)
{
    ui->spinPoints->setValue(value);
}

int AcquisitionOptions::getBaselineIters() const
{
    return ui->spinIter->value();
}

void AcquisitionOptions::setBaselineIters(int value)
{
    ui->spinIter->setValue(value);
}

int AcquisitionOptions::getBaselineOrder() const
{
    return ui->spinOrder->value();
}

void AcquisitionOptions::setBaselineOrder(int value)
{
    ui->spinOrder->setValue(value);
}

void AcquisitionOptions::saveSettings(QSettings &st) const
{
    st.setValue("deviceName"        , getDeviceName());
    st.setValue("exposureTime"      , getExposureTime());
    st.setValue("exposureWait"      , getExposureWait());
    st.setValue("baselineOrder"     , getBaselineOrder());
    st.setValue("baselineIterations", getBaselineIters());
    st.setValue("peakRising"        , getPeakRising());
    st.setValue("peakFalling"       , getPeakFalling());
    st.setValue("peakPoints"        , getPeakPoints());
    st.setValue("selectedFBGs"      , getSelectedFBGs());

    double min, max;
    getRange(min, max);
    st.setValue("rangeMin", min);
    st.setValue("rangeMax", max);
}

void AcquisitionOptions::loadSettings(QSettings &st)
{
    setDeviceName(st.value("deviceName"           , DEVICE_NAME).toString());
    setExposureTime(st.value("exposureTime"       , EXPOSURE_TIME_MS).toDouble());
    setExposureWait(st.value("exposureWait"       , EXPOSURE_WAIT_MS).toDouble());
    setBaselineOrder(st.value("baselineOrder"     , BASELINE_ORDER).toInt());
    setBaselineIters(st.value("baselineIterations", BASELINE_ITERS).toInt());
    setPeakRising(st.value("peakRising"           , PEAK_RISING).toDouble());
    setPeakFalling(st.value("peakFalling"         , PEAK_FALLING).toDouble());
    setPeakPoints(st.value("peakPoints"           , PEAK_POINTS).toInt());
    setSelectedFBGs(st.value("selectedFBGs"       , DEFAULT_FBGS).toString());

    double min, max;
    min = st.value("rangeMin", RANGE_MIN).toDouble();
    max = st.value("rangeMax", RANGE_MAX).toDouble();
    setRange(min, max);
}

void AcquisitionOptions::setDeviceName(const QString &value)
{
    ui->lineDeviceName->setText(value);
}

void AcquisitionOptions::setSelectedFBGs(const QString &value)
{
    ui->lineFBGs->setText(value);
}

void AcquisitionOptions::setRange(double min, double max)
{
    ui->spinMin->setValue(min);
    ui->spinMax->setValue(max);
}

QVector<int> AcquisitionOptions::getSelectedFBGsVector(int num_total) const
{
    QVector<int> fbgs;
    const QStringList list = getSelectedFBGs().split(",",QString::SkipEmptyParts);
    bool ok = true;

    foreach(QString num, list)
    {
        const int f = num.toInt(&ok) - 1;
        if( (f < 0) || (f >= num_total) )
            ok = false;

        if(!ok) break;
        fbgs.append(f);
    }

    // in case of error, use all
    if(!ok) {
        fbgs.clear();

        for(int i = 0; i < num_total; ++i)
            fbgs.append((i));
    }

    return fbgs;
}

void AcquisitionOptions::getRange(double &min, double &max) const
{
    min = ui->spinMin->value();
    max = ui->spinMax->value();
}

void AcquisitionOptions::checkRange(double value)
{
    Q_UNUSED(value);
    double min, max;
    getRange(min, max);

    if( (max - min) < 1)
    {
        max = min + 1;
        ui->spinMax->setValue(max);
    }
}
