#ifndef ACQUISITIONOPTIONS_H
#define ACQUISITIONOPTIONS_H

#include <QGroupBox>

class QSettings;

namespace Ui
{
class AcquisitionOptions;
}

/**
 * @brief Handles and stores all options related to signal acquisition.
 */
class AcquisitionOptions : public QGroupBox
{
    Q_OBJECT

public:
    explicit AcquisitionOptions(QWidget *parent = 0);
    ~AcquisitionOptions();

    QString getDeviceName()     const;
    QString getSelectedFBGs()   const;
    double  getPeakRising()     const;
    double  getPeakFalling()    const;
    double  getExposureTime()   const;
    double  getExposureWait()   const;
    double  getCycleTime()      const;
    double  getTotalTime()      const;
    int     getPeakPoints()     const;
    int     getBaselineOrder()  const;
    int     getBaselineIters()  const;
    void    getRange(double& min, double& max) const;

    void saveSettings(QSettings& st) const;
    void loadSettings(QSettings& st);

    void setDeviceName(const QString& value);
    void setSelectedFBGs(const QString& value);
    void setPeakRising(double value);
    void setPeakFalling(double value);
    void setExposureTime(double value);
    void setExposureWait(double value);
    void setPeakPoints(int value);
    void setBaselineOrder(int value);
    void setBaselineIters(int value);
    void setRange(double min, double max);

    QVector<int> getSelectedFBGsVector(int num_total) const;

protected:
    void resizeEvent(QResizeEvent *e);

private slots:
    void fixOddPoints();
    void checkRange(double value);

private:
    Q_DISABLE_COPY(AcquisitionOptions)
    Ui::AcquisitionOptions *ui;

};

#endif // ACQUISITIONOPTIONS_H
