#ifndef GENERALOPTIONS_H
#define GENERALOPTIONS_H

#include <QGroupBox>

class QSettings;
class QLineEdit;

namespace Ui
{
class GeneralOptions;
}

/**
 * @brief Handles and stores options related to signal recording.
 */
class GeneralOptions : public QGroupBox
{
    Q_OBJECT

public:
    explicit GeneralOptions(QLineEdit* expName_, QWidget *parent = 0);
    ~GeneralOptions();

    bool getRawSpectra()    const;
    bool getUseCurrentDir() const;

    void setRawSpectra(bool value);
    void setUseCurrentDir(bool value);

    void loadSettings(QSettings& st);
    void saveSettings(QSettings& st) const;

    void setSolverParams(const QVector<double>& params);
    QVector<double> getSolverParams() const;

    void setSolverLabels(const QVector<QString>& labels);

    void setRecordingOptionsEnabled(bool value);

    QString getExperimentPath() const;

private:
    Q_DISABLE_COPY(GeneralOptions)

    Ui::GeneralOptions *ui;
    QLineEdit* expName;
};

#endif // GENERALOPTIONS_H
