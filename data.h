#ifndef DATA_H
#define DATA_H

#include <QVector>
#include "scaleinfo.h"

/**
 * @brief Acquired data. Supports operations like scaling and peak detection.
 * Application should write / read directly from x and y.
 *
 * The common workflow is to create the Data object with a given number of points,
 * pass it to the function that will acquire the signal, scale the axes, remove the
 * baseline and then perform peak detection. Scaling and baseline removal are both
 * optional.
 *
 * Peak detection and baseline removal can be slow in debug mode.
 */
class Data
{
public:
    Data(int n_points);

    void calcBounds();
    void scaleX(const ScaleInfo &to);
    void scaleY(const ScaleInfo &to);

    /**
     * Computes the mean value on the y axis.
     */
    double meanValue() const;

    /**
     * Returns the peak wavelengths detected in the data. First, it detects the regions that
     * contains the peaks, then it find the maximum point of each region and perform a gaussian fit
     * on the surroundings.
     *
     * @param rising_thres  Threshold (proportional to the mean count) for the start of a peak region
     * @param falling_thres Threshold (proportional to the mean count) for the end   of a peak region
     * @param n_points Total number of points to use in the gaussian fitting
     */
    QVector<double> detectPeaks(double rising_thres, double falling_thres, int n_points) const;

    /**
     * Identifies and removes the baseline of the signal (y axis).
     * The baseline is assumed to be a polynomial with a given order. Performs a iterative
     * procedure where the signal is fitted by a polynomial that tries to avoid fitting the
     * signal peaks.
     */
    void removeBaseline(int order, int max_it = 100);

    /**
     * Crops the data to the [min, max] range.
     */
    void setRange(double min, double max);

    /**
     * Guarantees that the size of x and y are equal to size.
     */
    void checkSize(int size);

    QVector<double> x, y;

private:
    /**
     * Scaled the data on the y axis according to the from -> to information.
     */
    void scale(QVector<double> &data, const ScaleInfo &from, const ScaleInfo &to) const;

    /**
     * Computes the minimum and maximum values on data.
     */
    ScaleInfo calcBounds(const QVector<double> &data) const;

    /**
     * Returns the index with the peak count in the a to b (including) range
     */
    int findMaximum(int a, int b) const;

    /**
     * Returns the peak wavelength on the a to b (including) range by
     * fitting a gaussian to the data
     */
    double fitGaussian(int a, int b) const;

    ScaleInfo x_from, x_to, y_from, y_to;
};

#endif // DATA_H
